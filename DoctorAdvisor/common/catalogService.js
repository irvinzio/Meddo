commonApp.service('CatalogService', catalogService);

function catalogService($http,$q,Constants){
    var baseurl = Constants.BaseUrl;
    return{
        getUniversities: function(){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/2').then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getUniversityById: function(id){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/2/'+id).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getSpecialties: function(){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/3').then(function(resp){
              defered.resolve(resp);
            },function(err) {
              console.log(err);
              defered.reject(err);
            });
            return defered.promise;
        },
        getSpecialtyById: function(id){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/3/'+id).then(function(resp){
              defered.resolve(resp);
            },function(err) {
              console.log(err);
              defered.reject(err);
            });
            return defered.promise;
        },
        getIdTypes: function(){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/1').then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getIdTypeById: function(id){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/1/'+id).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getOfficeTypes: function(){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/5').then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getOfficeTypeById: function(id){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/5/'+id).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        }
    }
}