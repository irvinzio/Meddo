function getHeadersConfig(){
    return  {
        headers:  {
            'Authorization': 'JWT '+localStorage.token,
            'Content-Type': 'application/json'
        }
    }
}
function verifyUserLogged(){
    console.log("Esta verificando usuario");
    console.log(localStorage);
    if(localStorage.token == null || localStorage.userId == null )
        window.location.href = "/#!/login";
    return true;
}
function verifyPatientLogged(){
    console.log("Esta verificando paciente");
    console.log(localStorage);
    if(localStorage.token == null || localStorage.userId == null || localStorage.role != 'patient')
        return false;
    return true;
}