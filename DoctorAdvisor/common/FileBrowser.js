commonApp.factory('loadThumbnail', function ($window, $q) {
    'use strict';
    return function (file,thumbnailId,width,height) {
        var deferred = $q.defer(),  
        reader = new $window.FileReader();

        reader.onload = function (ev) {
            var content = ev.target.result;
            $('.'+thumbnailId)
                .attr('src', ev.target.result)
                .width(width)
                .height(height);
            deferred.resolve(content);
        };
        reader.readAsDataURL(file);
        return deferred.promise;
    };
})
.factory('UploadImage', function ($http,$window, $q, Constants){
    return function (fileInput,userType) {
        var baseurl = Constants.BaseUrl;
        var config = { 
            headers: {
                'Authorization': 'JWT '+localStorage.token, 
                'Content-Type': undefined 
            },
            transformResponse: angular.identity
        };
        var defer = $q.defer();
        var fd = new FormData();
        fd.append('imgUploader', fileInput);
        $http.post(baseurl+'/'+userType+'/upload', fd,config).then(function(resp){
            defer.resolve(resp);
        },function(err) {
            console.log(err);
            DA.main.sideAlert("Hubo un error subiendo la imagen intenta de nuevamente");
            defer.reject(err);
        });
        return defer.promise;
    };
})
.directive('fileBrowser', function (loadThumbnail,UploadImage) {
    'use strict';
    return {
        template: '<div>' +
                    '<input type="file" style="display: none;" />' +
                    '<a>{{caption}}</a>' +
                    '</div>',
        restrict:"E",
        require:  [
             'model',
             'idsrc',
             'iwidth',
             'iheight',
             'caption',
             'usertype'
        ],
        scope: {
            model : '=',
            idsrc : '@',
            iwidth : '@',
            iheight: '@',
            caption: '@',
            usertype: '@'
          },
        controller: function ($scope, $element,$attrs) {
            $scope.caption = $attrs.button;
            var fileInput = $element.children('div').children('input[file]');
            fileInput.on('change', function (event) {
                var file = event.target.files[0];
                loadThumbnail(file,$scope.idsrc,$scope.iwidth,$scope.iheight).then(function (content) {
                    console.log("Tumbnail loaded");
                });
                UploadImage(file,$scope.usertype).then(function (content) {;
                    $scope.model = JSON.parse(content.data).uuid;
                });
            });
            $element.on('click', function () {
                fileInput[0].click();
            });
        }
    };
});