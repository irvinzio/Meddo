commonApp.service('locationService', locationService);

function locationService($http,$q,Constants){
    var battutaApiKey = Constants.battutaApiKey;
    var baseurl = Constants.BaseUrl;    
    return{
        getPhoneCodes: function(){      
            return [{id:1,name:"+52"}];
        },
        getStates: function(){ 
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/8').then(function(resp){
                resp.data.unshift({id: 0, type: 0, id_type: 0, name: "Seleccionar Estado", visible: 1});   
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getStateById: function(id){ 
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/8/'+id).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getCities: function(){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/9').then(function(resp){
                resp.data.unshift({id: 0, type: 0, id_type: 0, name: "Seleccionar Ciudad", visible: 1});
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getCityById: function(id){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/9/'+id).then(function(resp){
                resp.data.unshift({id: 0, type: 0, id_type: 0, name: "Seleccionar Ciudad", visible: 1});
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getCitiestByStateId: function(id){
            var defered = $q.defer();
            $http.get(baseurl+'/catalog/cities/'+id).then(function(resp){
                resp.data.unshift({id: 0, type: 0, id_type: 0, name: "Seleccionar Ciudad", visible: 1});
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        buildPhoneNumber: function(phone){
            return phone.code +" "+phone.number;
        }
    }
}