commonApp.service('DateService', dateService);

function dateService(){
    var months = [{id:0,name:"Enero"},{id:1,name:"Febrero"},{id:2,name:"Marzo"},{id:3,name:"Abril"},{id:4,name:"Mayo"},{id:5,name:"Junio"},{id:6,name:"Julio"},{id:7,name:"Agosto"},{id:8,name:"Septiembre"},{id:9,name:"Octubre"},{id:10,name:"Noviembre"},{id:11,name:"Diciembre"}];
    var weekDays = [{id:0,name:"Domingo"},{id:1,name:"Lunes"},{id:2,name:"Martes"},{id:3,name:"Miercoles"},{id:4,name:"Jueves"},{id:5,name:"Viernes"},{id:6,name:"Sabado"}]
    return{
        getDays: function(year,month){
            var monthTotalDays = new Date(year,month,0).getDate();
            var days = [];
            for(i=1;i<monthTotalDays;i++){
                days.push(i);
            }  
            return days;
        },
        getWeekDays : function(){
            var curr = new Date(); // get current date
            var month = curr.getMonth();
            var year = curr.getFullYear();
            var monthTotalDays = new Date(year,month,0).getDate();

            var firstDayOfweek = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
            var lastDayOfWeek = firstDayOfweek + 6; // last day is the first day + 6
            for(var i=0;i<7;i++){
                if(firstDayOfweek<=monthTotalDays){
                    weekDays[i].day = firstDayOfweek + i;
                    weekDays[i].month = month;
                    weekDays[i].year = year;
                }  
                else {
                    firstDayOfweek = 0;
                    weekDays[i].day = firstDayOfweek + i;
                    weekDays[i].month = month;
                    weekDays[i].year = year;
                }
            }
            return weekDays;
        },
        setWeekDays : function(year,month,day){
            var monthTotalDays = new Date(year,month,0).getDate();
            var firstDayOfweek = day;

            for(var i=0;i<7;i++){
                if(firstDayOfweek>=monthTotalDays){
                    firstDayOfweek = 1;
                    month++;
                }  
                else if(month>=11) {
                    firstDayOfweek = 1;
                    month = 0;
                    year++;
                }
                weekDays[i].day = firstDayOfweek;
                weekDays[i].month = month;
                weekDays[i].year = year;
                firstDayOfweek ++;
            }
            return weekDays;
        },
        getDaysInMonth : function(month, year) {
            return new Date(year, month, 0).getDate();
        },
        getMonths: function(){
            return months;
        },
        getMonthsById: function(id){
            return months[0];

        },
        getYears: function(){
            var date = new Date();
            var currentYear = date.getFullYear();
            var years = [];
            for(i=1900;i<currentYear;i++){
                years.push(i);
            }  
            return years;
        },
        getReadableDate : function getMonth(d) {
            var m = this.getMonths();
            var date = new Date(d);
            var year = date.getFullYear();
            var month = date.getMonth();
            var day = date.getDate();
            var fullDay = day < 10 ? '0' + day : '' + day;
            return fullDay+" de "+months[month].name+" de "+year;
        }  
    }
}