var express = require('express');

var app = express();

var port = process.env.PORT || 3000;

var path = require('path');

var router = express.Router();

const cors = require('cors');

var fetch = require('node-fetch');


router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, application/json");
    res.header("X-Requested-With", "XMLHttpRequest");
    next();
  }); 
// app.use(function (req, res, next) {
//     var options = {
//        method : req.method,
//        headers :req.headers,
//        body : JSON.stringify(req.body)
//    }; 

//    fetch('http://127.0.0.1:3000'+req.path,options)
//    .then(response => {         
//        return response.json();
//    }, error => { console.error(error)})
//    .then(function(json){
//        res.send(json);
//    })
//    .catch(function(error) { 
//       res.send(error); 
//    });
// });

  
app.use('/', express.static(__dirname + '/'));


// /* Server-side rendering */
// function angularRouter(req, res) {

//     /* Server-side rendering */
//     //res.render('index', { req, res });
//     console.log(__dirname + '/index.html');
//     res.sendFile(path.join(__dirname + '/Doctor/index.html'));
  
//   }

// app.get('/Doctor/*', angularRouter);

// app.get('/',function(req,res){
//     res.send('welcome to my API');
// });

app.listen(port,function(){
  console.log('Running on PORT' + port);
});