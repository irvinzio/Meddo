/* global DA, $ */
DA.utils = (function () {
    var _avatar_listener = function avatar_listener( input, endpoint, preview, message ) {

        if (input.addEventListener) {
            input.addEventListener('change', function ( event ) {
                var i = 0,
                    formdata = new FormData(),
                    length = this.files.length,
                    img = false,
                    file;

                formdata.append("action", "update_avatar");

                for ( ; i < length; i++ ) {
                    file = this.files[i];
                    if (!!file.type.match(/image.*/)) {
                        formdata.append("image", file);
                        img = true;
                        break;
                    }
                }

                if ( img ) {
                    message.innerText = 'Subiendo';
                    $.ajax({
                        url: endpoint, // Url to which the request is send
                        type: 'POST',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function ( json_data ) {
                            var _$avatar_preview = $( preview );
                            _$avatar_preview.attr({src:json_data.src}).hide();
                            setTimeout(function () {
                                _$avatar_preview.fadeIn();
                            }, 500);
                            message.innerText = 'Avatar actualizado';
                        }
                    });
                }else{
                    message.innerText = 'Elije una image';
                }
            }, false);
        }
    };
    var _image_gallery = function _image_gallery( input, endpoint, callback ) {
        if (input.addEventListener) {
            input.addEventListener('change', function ( event ) {
                var i = 0,
                    formdata = new FormData(),
                    length = this.files.length,
                    img = false,
                    file;

                formdata.append("action", "upload_image");

                for ( i = 0; i < length; i++ ) {
                    file = this.files[i];
                    if (!!file.type.match(/image.*/)) {
                        formdata.append("image", file);
                        img = true;
                        break;
                    }
                }

                if ( img ) {
                    $.ajax({
                        url: endpoint, // Url to which the request is send
                        type: 'POST',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function ( json_data ) {
                            if( callback ){
                                callback( json_data );
                            }
                        }
                    });
                }
            }, false);
        }
    };


    var _autocompletes = {};
    function _initAutocomplete( id_str, callback_func) {
        var target =  document.getElementById( id_str );
        if( target ){
            _autocompletes[id_str] = new google.maps.places.Autocomplete(
                document.getElementById( id_str ),{
                    types: ['geocode'],
                    componentRestrictions: {
                        country: 'mx'
                    }
                }
            );
            _autocompletes[id_str].addListener('place_changed', function () {
                callback_func( id_str );
            });
        }
    }
    var _updateLL = function _updateLL( id_prefix ) {
        var latitude = parseFloat(  document.getElementById( id_prefix + '-latitude' ).value ),
            longitude = parseFloat(  document.getElementById( id_prefix + '-longitude' ).value ),
            ll = latitude+','+longitude,
            image = document.getElementById(  id_prefix + '-map-preview' ),
            key = image.dataset.key;
        if( latitude && longitude ){
            image.src  = 'https://maps.googleapis.com/maps/api/staticmap?center='+ll+'&key='+key+
                '&size=800x180&markers=color:blue%7Clabel:S%7C'+ll;
        }
    };
    var _regularMap = function _regularMap( id_str ) {
        var place = _autocompletes[ id_str + '' ].getPlace(),
            address_params = [],
            ok = true,
            i = place.address_components.length,
            address_html = '',
            j;

        console.info( place.adr_address );
        while ( i > 0 ) {
            i--;
            ok = true;
            for( j in place.address_components[i].types ){

                if(
                    place.address_components[i].types[j] === 'locality'
                ){
                    var search_slug = DA.utils.slug( place.address_components[i].long_name );
                    $('#state_id > option').each(function () {
                        var $opt = $(this),
                            my_slug =  DA.utils.slug( $opt.text() );
                        if( search_slug === my_slug ){
                            $('#state_id').val( $opt.val() );
                            $('#state_id').trigger('change');
                            return false;
                        }
                    });
                    break;
                }
                if(
                    place.address_components[i].types[j] === 'country'
                    || place.address_components[i].types[j] === 'postal_code'
                ){
                    ok = false;
                    break;
                }
                if( place.address_components[i].types[j] === 'street_number' ){
                    address_params[address_params.length - 1 ] = address_params[address_params.length - 1 ]+' ' +place.address_components[i].long_name;

                    ok = false;
                    break;
                }
            }
            if( ok ){
                address_params.push(place.address_components[i].long_name)
            }
        }
        //          address_html = '<p>'+( address_params.join('</p><p>') )+'</p>';

        if( place.hasOwnProperty('adr_address') ){
            address_html = '<p>'+ place.adr_address +'</p>';
        }

        $('#'+ id_str + '-address').trumbowyg('html', address_html);


        document.getElementById( id_str + '-latitude' ).value = place.geometry.location.lat();
        document.getElementById( id_str + '-longitude' ).value = place.geometry.location.lng();
        _updateLL( id_str );
    };
    var _initMapListener = function _initMapListener( id ) {
        _initAutocomplete( id , _regularMap);
        _updateLL( id );
    };
    var _initMaps = function _initMaps() {
        var autocomplete_maps = document.getElementsByClassName('map-autocomplete'),
            ii = autocomplete_maps.length;
        while( ii > 0 ){
            ii--;
            _initMapListener( autocomplete_maps[ii].id );
        }
    };
    var _slug = function _slug( str ){
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";

        for (var i=0, l=from.length ; i<l ; i++)
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));


        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    };

    return {
        commas: function commas( x ) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        avatar_listener: function avatar_listener( input, endpoint, preview, message ) {
            return _avatar_listener( input, endpoint, preview, message );
        },
        image_gallery_listener: function image_gallery_listener( input, endpoint, callback ) {
            return _image_gallery( input, endpoint, callback );
        }, slug: function slug( str ){
            return _slug( str );
        },
        initMaps: function initMaps() {
            _initMaps();
        }
    }
})();