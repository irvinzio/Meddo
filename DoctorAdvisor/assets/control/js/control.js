/* global DA, $ */
DA.control = (function () {
    function _initVars() {

    }
    var _$tables = {};
    function _loadTable( $table ) {

        var DT = window.DATA_TABLES ? window.DATA_TABLES : {};
        var params = {};

        if( $table.data('ajax') ){
            params.ajax = $table.data('ajax');
        }
        params.deferRender = true;
        params.processing = true;
        params.lengthMenu = [[50, 100, -1],[50, 100, "All"]];

        if( !$.isEmptyObject( DT ) ){
            if( DT.hasOwnProperty('columns')){
                params.columns = DT.columns;
            }
            if( DT.hasOwnProperty('columns')){
                params.order = DT.order;
            }
        }
        console.info( "Data tables:", params );
        _$tables[ $table.prop('id') ] = $table.DataTable(params);

    }
    function _initEvents() {

        // Init data tables
        $('table.data-table').each(function () {
            _loadTable( $(this) );
        });

        $('.load-result-by-group').delegate('button', 'click', function () {

            var $btn = $(this),
                ajax = $btn.data('ajax'),
                $target = _$tables[ $btn.data('target') ],
                $wrapper = $btn.closest('.load-result-by-group'),
                $btns = $wrapper.find('button');
            $btns.removeClass('active');
            $btn.addClass('active');
            $target.ajax.url( ajax ).load();

        });
        $('textarea.html-edit').trumbowyg({
            btns: [['bold', 'italic'], ['link'], 'btnGrp-lists'],
            autogrow: true
        });

        $('.item-gallery').each(function () {
            var $gallery = $(this),
                endpoint = $gallery.data('endpoint'),
                $gallery_wrapper = $gallery.children('.gallery-wrapper'),
                add_image_input = document.getElementById('gallery-image')
            ;


            $gallery.delegate('button.add-element-video', 'click', function ( event ) {
                event.preventDefault();
                var yt_id = prompt('Ingresa el ID de youtube');
                if( yt_id ){
                    var post_data = {
                        'action' : 'upload_video',
                        'id': yt_id
                    };
                    $.ajax({
                        data: post_data,
                        url: endpoint,
                        type: 'POST',
                        success: function ( json_data ) {
                            refresh_gallery();
                        }
                    });
                }
            });
            function refresh_gallery() {
                $gallery.css({
                    'pointer-events':'none',
                    'opacity': 0.3
                });
                $.ajax({
                    success: function (json_data) {
                        var html = '',
                            id = 'gallery-' + parseInt( (new Date)*1, 10),
                            render_data,
                            ii, item;

                        for( ii in json_data.data ){
                            item = json_data.data[ii];
                            if( item.type == 'video' ){
                                html += Mustache.render( DA.templates.item_gallery_video, item);
                            }else{
                                html += Mustache.render( DA.templates.item_gallery_image, item);
                            }
                        }

                        render_data = {
                            id: id,
                            html: html
                        };
                        html = Mustache.render( DA.templates.item_gallery, render_data);
                        $gallery.css({ 'pointer-events' : 'all' }).stop().animate({ 'opacity' : 1}, 300);
                        $gallery_wrapper.html( html );


                        $('#'+id)
                            .delegate('button.delete-element', 'click', function ( event ) {
                                event.preventDefault();
                                if( confirm('¿Estás seguro?, esta acción no se puede deshacer.') ){
                                    var $this = $(this),
                                        id = $this.data('id');
                                    $.ajax({
                                        success: function success() {
                                            refresh_gallery();
                                        },
                                        url: '/control/services/delete_from_gallery/'+id
                                    })
                                }
                            })
                            .owlCarousel({
                                items:2,
                                loop:true,
                                margin:5,
                                height: 250,
                                nav:true,
                                video:true,
                                lazyLoad:true,
                                center:true
                            });

                    },
                    url: endpoint
                });
            }
            DA.utils.image_gallery_listener( add_image_input, endpoint, refresh_gallery);
            refresh_gallery();
        });

    }
    function _initLayout() {

        // Bueger Icon
        $('.hamburger-menu').on('click', function( event ) {
            event.preventDefault();
            $(this).toggleClass('active');
            $('body').toggleClass('sidebar-toggled');
        });

    }
    function _init() {

        var _token =  $('meta[name="csrf-token"]').attr('content');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': _token
            }
        });
        $('form').each(function ( ) {
            var $this = $(this),
                $token = $this.find('*[name="_token"]');
            if( $token.length === 0 ){
                $this.append('<input name="_token" type="hidden" value="'+_token+'" />');
            }
        });
        _initVars();
        _initEvents();
        _initLayout();
    }
    return {
        init: function init() {
            _init();
        }
    };
})();