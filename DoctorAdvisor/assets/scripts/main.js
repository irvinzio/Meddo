/* global DA, $ */
// var DA={};
DA.main = (function(){
    var
        _$body,
        _$side_alert,

        _$stars_to_render,
        _$goToId,

        _$tab_panel,
        _$tab_panel_element,
        _$tab_panel_head,

        _$html_text,

        _$triggerFancy,
        _$closeFancy,
        _$fancy_zone,

        _$site_description,
        _$mobile_header,
        _$mobile_header_button,

        _$report_profile_problem,
        _$report_problem_ul,
        _$report_problem_li,

        _$new_user_trigger,
        _$register_zone,
        _$prb,
        _$lsb,

        _$select_multiple,
        _$select_multiple_ajax,
        _$money,

        _$contact_profile,

        _$global_style_form,

    _textRating = ['Pesimo', 'Malo', 'Regular', 'Bueno', 'Excelente'];


    var _initVars = function _initVars(){
        _$body = $('body');
        _$side_alert = $('.side-alert');

        _$select_multiple = $("select.select-multiple");
        _$select_multiple_ajax = $("select.select-multiple-ajax");

        _$stars_to_render = $(".stars-inline.to-render, .star-wrapper.to-render");

        _$goToId = $(".goToId");

        _$tab_panel = $(".tab-panel");
        _$tab_panel_head = $(".tab-panel .head");

        _$html_text = $('.html-text');

        _$triggerFancy = $(".triggerFancy");
        _$closeFancy = $(".fancy-zone .fancy-bg, .fancy-zone .fancy-close");
        _$fancy_zone = $(".fancy-zone");

        _$mobile_header = $(".mobile-header");
        _$mobile_header_button = $(".mobile-header .right");

        _$site_description = $(".site-description");
        _$report_profile_problem = $("#report-profile-problem");
        _$report_problem_ul = $(".report-problem");
        _$report_problem_li = $(".report-problem li");

        _$new_user_trigger = $("a.new-user-trigger");
        _$register_zone = $(".please-login .register-zone");

        _$prb = $('#password-recovery-button');
        _$lsb = $('#login-send-button');

        _$contact_profile = $(".contact-profile");

        _$global_style_form = $("form.global-style");
        _$money = $('.money');

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token' : $('meta[name=csrf-token]').attr('content')
            }
        });
    };
    function _formatResult (result){
        console.info( result );
        if (result.loading){
            return 'Buscando ...';
        }

        var html =  '<div class="select2-html-option">';

        if( result.avatar  ){
            html += '<img src="' + result.avatar + '">' +
                '<h4>'+result.name+'</h4>';
        }else if( $(result.element).length > 0 ){
            html += '<img src="' + $(result.element).data('avatar') + '">' +
                '<h4>'+$(result.element).text()+'</h4>';
        }else{
            html += '<h4>'+result.text+'</h4>'
        }

        html +=  '</div>';
        return html;
    }

    var _initEvents = function _initEvents(){

        if( _$money.length > 0 ){
            _$money.maskMoney({
                prefix: '$ ',
                allowZero: false,
                allowNegative: false,
                selectAllOnFocus: true,
                affixesStay: true
            });
            _$money.trigger('mask.maskMoney');
        }

        if(_$select_multiple.length > 0 ){
            _$select_multiple.select2();
        }
        if(_$select_multiple_ajax.length > 0 ){
            _$select_multiple_ajax.select2({
                // tags: true,
                multiple: true,
                //tokenSeparators: [',', ' '],
                minimumInputLength: 3,
                templateResult : _formatResult,
                templateSelection : _formatResult,
                escapeMarkup: function(m) {
                    // Do not escape HTML in the select options text
                    return m;
                },
                ajax: {
                    // url: URL,
                    dataType: "json",
                    type: "GET",
                    processResults: function (json_result) {
                        return {
                            results: json_result.items
                        };
                    }
                }
            }).trigger('change');;

        }
        // $.get('/services/autocomplete/all.json', function(json_data){
        //     $('.search-value').each(function () {
        //         var $this = $(this);
        //         $this.typeahead({
        //             source: json_data,
        //             autoSelect: true
        //         });
        //         $this.change(function() {
        //             var current = $this.typeahead("getActive").toLowerCase(),
        //                 value = $this.val().toLowerCase(),
        //                 $form, $input;

        //             if( current === value ){
        //                 if( $this.hasClass('auto-search') ){
        //                     $form = $this.closest('form');
        //                     $form.trigger('submit');
        //                 }else if( $this.hasClass('to-input') ){
        //                     $input = $('#'+$this.data('next'));
        //                     $input.focus();
        //                 }
        //             }else{
        //                 console.info( '¿Quisiste decir '+current+'?');
        //             }
        //             // auto-search
        //             // data-next="autocomplete"
        //         });
        //     });

        // },'json');

        _$mobile_header_button.click( function(){
            _$mobile_header.toggleClass('active');
        });

        if( _$stars_to_render.length > 0 ){
            _$stars_to_render.each(function () {
                var $this = $(this),
                    extra_class = $this.data('class'),
                    my_score = Math.round( $this.data('score') ),
                    max_score = 5,
                    ii = 0;

                var html = '<div class="stars '+(extra_class?extra_class:'')+'">';

                while( ii < max_score){
                    ii++;
                    if( my_score >= ii ){
                        html += '<div class="star rated-star"></div>';
                    }else{
                        html += '<div class="star"></div>';
                    }
                }
                html += '</div>';
                $this.removeClass('to-render').html( html );

            });
        }

        _$report_profile_problem.click( function (event) {
            event.preventDefault();
            $(this).find('ul').toggleClass('active');
        });

        if( _$tab_panel.length > 0 ){
            setTimeout(function(){
                $.each($(".tab-panel .body .overflow"), function () {
                    var $this  = $(this);
                    $this.closest(".body").css({
                        height: $this.height() + 50
                        // maxHeight: $this.height() + 50
                    }).attr('data-height', $this.height() + 50);
                });
                $(".tab-panel").addClass('active');
                // $(".element.open .body").css({
                //     'max-height': parseInt($(this).attr('data-height'))
                // })
            }, 300);

            setTimeout(function () {
                $(".element.open .head").trigger('click');
            }, 600);

            _$tab_panel_head.click( function(){
                var $this  = $(this);
                _$tab_panel_element = $this.closest('.element');
                var _$tab_panel_element_body = _$tab_panel_element.find('.body'),
                    _$element_open = $(".element.open");
                _$element_open.find('.body').css({
                    'max-height': 0
                });
                _$element_open.removeClass('open');
                _$tab_panel_element_body.css({
                    'max-height': parseInt(_$tab_panel_element_body.attr('data-height'))
                });
                _$tab_panel_element.addClass('open');
            });

            _$tab_panel_head.find('.close').click( function () {
                setTimeout(function () {
                    var _$element_open = $(".element.open");
                    console.log(_$element_open);
                    _$element_open.find('.body').css({
                        'max-height': 0
                    });
                    _$element_open.removeClass('open');
                }, 100);
            });
        }

        if(_$html_text.length > 0){
            _$html_text.trumbowyg({
                btns: [['bold', 'italic'], ['link'], 'btnGrp-lists'],
                removeformatPasted: true,
                autogrow: true
            });
        }

        if(_$site_description.length > 0){
            setTimeout(function () {
                _$site_description.hide().removeClass('hidden').slideDown(450);
            }, 999);
        }

        _$goToId.click( function(event){
            event.preventDefault();
            console.log($(this).data('id'));
            $("html, body").animate({
                scrollTop: ($('#'+$(this).data('id')).offset().top - $('header').height())
            }, 1000);
        });
        if( window.location.hash ){
            var id = window.location.hash.split('#').join('');
            window.onload = function () {
                $('.goToId[data-id="'+id+'"]').trigger('click');
            };
        }

        if(_$triggerFancy){
            _$triggerFancy.click( function(event){
                event.preventDefault();
                _$fancy_zone.addClass('active');
            });
            _$closeFancy.click( function(event){
                event.preventDefault();
                _$fancy_zone.removeClass('active');
            });
        }

        _$report_problem_li.click( function () {
            var $this = $(this);
            $("#problem-type").val($this.text());
            $("#fancy-report-problem").addClass('active');
            // console.log($this.text());
        });

        _$prb.click( function (event) {
            _$prb.hide();
            $('#login-password-wrapper').slideUp(300);
            $('#login-send-button').text('Recuperar contraseña');

            $('#login-recovery-form').removeClass('form-login-user').addClass('form-recovery-password');

            // class="global-style " id=""

        });
        _$new_user_trigger.click( function (event) {
            event.preventDefault();
            _$register_zone.find('.buttons').slideUp(300);
            var $this = $(this),
                type = $this.hasClass('style-patient')?'patient':'doctor-hospital';
            if( type === 'doctor-hospital' ){
            }else{
            }
            $('#new-user-type').val( type );
            $('.btn.facebook').attr({'href':'/login/facebook?type='+type});

            setTimeout(function () {
                _$register_zone.find('.form-register-new-user').slideDown(300);
                _setInputsWidth();
            }, 300);
        });

        _$body
            .delegate('form.update-profile-data', 'submit', function (event) {
                var $form = $(this),
                    password = $form.find('[name="password"]').val(),
                    password_confirm = $form.find('[name="password_confirm"]').val(),
                    name = $form.find('[name="name"]').val(),
                    last_name = $form.find('[name="last_name"]').val(),
                    errors = [];

                if( name && last_name){
                    if( password ){
                        if( password === password_confirm ){

                        }else{
                            errors.push('Los passwords deben ser iguales');
                        }
                    }
                }else{
                    errors.push('Ingresa tu nombre completo');
                }
                if( errors.length !== 0 ){
                    event.preventDefault();
                    DA.main.sideAlert( errors[0] );
                }
            })
            .delegate('form-recovery-password', 'submit', function (event) {
                event.preventDefault();
                var $form = $(this),
                    email = $form.find('[name="email"]').val(),
                    errors = [],
                    post_data;

                if( email ){

                }else{
                    errors.push('Ingresa tu correo electrónico');
                }
                if( errors.length === 0 ){

                    post_data = {
                        'email' : email
                    };

                    DA.utils.ajaxForm( post_data, '/services/recover_password', function callback() {
                        DA.main.sideAlert( 'Revisa tu correo' );
                        $form.find('[name="email"]').val('')
                    });
                }else{
                    DA.main.sideAlert( errors[0] );
                }
            })
            .delegate('form.deactivate-account', 'submit', function (event) {
                event.preventDefault();
                var $form = $(this),
                    $email = $form.find('[name="email"]'),
                    expected = $email.data('email').toLowerCase(),
                    written = $email.val().toLowerCase(),
                    errors = [],

                    post_data;
                
                if( written ){
                    if( expected === written ){
                        
                    }else{
                        errors.push('Escribe el mismo correo que diste de alta');
                    }
                }else{
                    errors.push('Escribe tu correo electrónico');
                }
                if( errors.length === 0 ){
                    post_data = {
                        'email' : written
                    };

                    DA.utils.ajaxForm( post_data, '/services/suspend', function callback() {
                        DA.main.sideAlert( 'Tu cuenta será suspendida a la brevedad.' );
                        $email.val('');
                        document.location.href = '/logout';
                    });
                }else{
                    console.info( errors );
                    DA.main.sideAlert( errors[0] );
                }
                
            })
            .delegate('form.form-login-user', 'submit', function (event) {
                event.preventDefault();
                var $form = $(this),
                    email = $form.find('[name="email"]').val(),
                    password = $form.find('[name="password"]').val(),
                    errors = [],
                    post_data;

                if( !(email && password) )
                    errors.push('Ingresa tu correo y contraseña');
                // else
                //     DA.main.sideAlert( errors[0] );
            })
            .delegate('form.form-register-new-user', 'submit', function (event) {
                event.preventDefault();
                var $form = $(this),

                    name = $form.find('[name="name"]').val(),
                    last_name = $form.find('[name="last_name"]').val(),
                    email = $form.find('[name="email"]').val(),
                    password1 = $form.find('[name="password1"]').val(),
                    password2 = $form.find('[name="password2"]').val(),
                    errors = [],
                    post_data;

                if( !(name && last_name))
                    errors.push('Ingresa tu nombre completo');
                if( !email )
                    errors.push('Ingresa tu correo electrónico');
                if( password1 != password2)
                    errors.push('las contraseñas no coinciden');


                if( errors.length === 0 ){

                    // post_data = {
                    //     'type' : $form.find('[name="type"]').val(),
                    //     'name' : name,
                    //     'last_name' : last_name,
                    //     'email' : email,
                    //     'password' : password
                    // };
                    // DA.utils.ajaxForm( post_data, '/services/register', function callback() {
                    //     DA.main.sideAlert( 'Revisa tu correo para completar tu registro' );
                    //     $form.find('[name="name"]').val('');
                    //     $form.find('[name="last_name"]').val('');
                    //     $form.find('[name="email"]').val('');
                    //     $form.find('[name="password"]').val('');
                    // });

                }else{
                    DA.main.sideAlert( errors[0] );
                }
            })
            // .delegate('form.regular-contact-form', 'submit', function (event) {
            //     event.preventDefault();

            //     console.log(event);

            //     var $form = $(this),
            //         form_category = $form.data('category'),
            //         type = $form.find('[name="type"]').val(),
            //         topic = $form.find('[name="topic"]').val(),

            //         name = $form.find('[name="name"]').val(),
            //         last_name = $form.find('[name="last_name"]').val(),
            //         phone = $form.find('[name="phone"]').val(),
            //         email = $form.find('[name="email"]').val(),
            //         comment = $form.find('[name="comment"]').val(),

            //         endpoint = $form.data('endpoint'),
            //         errors = [],

            //         instance_type, instance_id;

            //     if(
            //         form_category === 'report'
            //         || form_category === 'contact'
            //     ){
            //         last_name = '-';
            //     }

            //     if( name && last_name){
            //         if( email ){
            //             if( comment ){

            //             }else{
            //                 errors.push('Ingresa tu comentario');
            //             }
            //         }else{
            //             errors.push('Ingresa tu correo electrónico');
            //         }
            //     }else{
            //         errors.push('Ingresa tu nombre completo');
            //     }

            //     if( errors.length === 0){

            //         var post_data = {
            //             'type' : type,
            //             'name' : name,
            //             'last_name' : last_name,
            //             'phone' : phone,
            //             'email' : email,
            //             'topic' : topic,
            //             'comment' : comment
            //         };

            //         if( form_category === 'contact' ){
            //             last_name = '-';
            //             post_data.instance_type = $form.data('instance_type');
            //             post_data.instance_id = $form.data('instance_id');
            //             post_data.to = $form.data('aditional-email');
            //         }
            //         DA.utils.ajaxForm( post_data, endpoint, function callback() {
            //             DA.main.sideAlert( '¡Gracias! te contactaremos a la brevedad' );
            //             $form.find('[name="name"]').val('');
            //             $form.find('[name="last_name"]').val('');
            //             $form.find('[name="phone"]').val('');
            //             $form.find('[name="email"]').val('');
            //             $form.find('[name="comment"]').val('');
            //             $(".fancy-zone").removeClass('active');
            //         });
            //     }else{
            //         DA.main.sideAlert( errors[0] );
            //     }
            // })
        ;

        $('textarea.html-edit').each(function () {
            $(this).trumbowyg({
                btns: [['bold', 'italic'], ['link'], 'btnGrp-lists'],
                autogrow: true
            });
        });


        _setInputsWidth();


        if(_$contact_profile){
            _$contact_profile.click( function (event) {
                event.preventDefault();
                $("#contacto-profile").addClass('active');
                var $this = $(this);
                var _data = $this.data('context');
                var copy = $("#copy-aux");

                var to = $this.data('aditional-email');
                if(to){
                    $("form.global-style.regular-contact-form").attr('data-aditional-email', to);
                }

                $("#subject-profile").val(_data);
                $("#profile-contact-name").text($this.data('name'));
                $("#profile-contact-image").css({
                    'background-image': 'url(' + $this.data('image' )+')'
                });
                if(_data === "Mandar mensaje"){
                    copy.text('Mensaje');
                    $("#aux-hidden").hide();
                }else{
                    $("#aux-hidden").show();
                    copy.text('Servicio del cual desea solicitar precio');
                }
            });
        }

        $(".triggerPhoneCall").click( function (event) {
            event.preventDefault();
            var phone_number = $(this).data('tel');
            console.log(phone_number);
            if($(window).width() <= 480){
                window.location.href="tel://"+phone_number;
            }else{
                $(this).closest('.single').html('').append('<span class="tel">'+ phone_number +'</span>');
            }
        });
    };

    var _setInputsWidth = function _setInputsWidth() {
        if( _$global_style_form.length > 0 ){
            $.each(_$global_style_form, function(){
                var $this = $(this);
                var inputs = $this.find('fieldset label input');
                if(inputs){
                    $.each(inputs, function (index, value) {
                        // console.log($(this).closest('label').width());
                        // console.log($(this));
                        // console.log($(this).position().left);
                        // console.log( $(this).closest('label').width() - $(this).position().left - 20 );
                        // console.log(" ------ ");
                        $(this).width( $(this).closest('label').width() - $(this).position().left - 20 );
                    });
                }
            });
        }
    };

    var _rateSystem = function _rateSystem(){
        var starRating = 0;

        $(".stars.vote .star")
            .hover(
                function(e) {
                    starRating = $(this).parent().find(".star.rated-star").length;
                    var hoveredStars = $(this).prevAll().add(this);
                    hoveredStars.addClass("hovered");
                    var allStars = $(this).siblings().add(this);
                    allStars.removeClass("rated-star");
                },
                function(e) {
                    var allStars = $(this).siblings().add(this);
                    allStars.removeClass("hovered unhovered");
                    allStars.slice(0, starRating).addClass("rated-star");
                }
            )
            .on("click", function(e) {
                var rateStars,
                    totalStars,
                    totalProm = 0,
                    totalPoints = 0,
                    $this = $(this),
                    allStars = $this.siblings().add(this),
                    hoveredStars = $this.prevAll().add(this),
                    $vote_input = $('#'+$this.closest('.stars-inline').data('input-id'))
                    ;
                console.info( $this );
                allStars.removeClass("rated-star");
                hoveredStars.addClass("rated-star");
                starRating = hoveredStars.length;


                rateStars = $(this).closest('.stars-inline').find('.stars.vote');
                totalStars = rateStars.length;
                $.each(rateStars, function(){
                    totalPoints += parseInt($(this).find('.star.rated-star').length);
                });
                $vote_input.val( totalPoints );
                totalProm = Math.floor((totalPoints * 5 / ( totalStars * 5) ));
                $(".stars.average").find('.star').removeClass('rated-star');
                for(i=0; i < totalProm; i++){
                    $(".stars.average").find('.star:eq('+ (i) +')').addClass('rated-star');
                }

                var wrapper = $this.closest('.rate-section-profile');
                if(wrapper){
                    wrapper.find('.rate-element.average .type span').text(_textRating[(totalProm - 1)]);
                    wrapper.find('.rate-element.average .type').removeClass('status-1 status-2 status-3 status-4 status-5').addClass('status-'+ totalProm);
                }

            });
    };

    var _sideAlert = function _sideAlert(message){
        var single_alert = document.createElement("div");
        var single_alert_message = document.createElement("div");
        single_alert.className = 'single-alert';
        single_alert_message.className = 'message';
        single_alert_message.innerHTML = message;
        single_alert.appendChild(single_alert_message);

        _$side_alert.append(single_alert);
        setTimeout(function () {
            single_alert.className = 'single-alert active';
        }, 100);
        setTimeout(function () {
            single_alert.className = 'single-alert';
        }, 5000);
        setTimeout(function () {
            single_alert.remove( );
        }, 5500);
    };


    return {
        init: function init() {
            _initVars();
            _initEvents();
            _rateSystem();
        },
        sideAlert: function sideAlert(message) {
            _sideAlert(message);
        }
    }
})();