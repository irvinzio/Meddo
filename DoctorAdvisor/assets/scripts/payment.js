DA.payment = (function () {

    var _stripe,
        _elements,
        _card;



    var _init = function _init( stripe_key ) {
        _stripe = Stripe( stripe_key  );
        _elements = _stripe.elements();
        _card = _elements.create('card', {
            style: {
                base: {
                    iconColor: '#237ec4',
                    color: '#033c67',
                    lineHeight: '40px',
                    fontWeight: 300,
                    fontFamily: '"Lato", Arial, sans-serif',
                    fontSize: '14px',
                    '::placeholder': {
                        color: '#CFD7E0'
                    }
                }
            }
        });
        _card.mount('#card-inputs');
        _card.on('change', function(event) {
            setOutcome(event);
        });


        $('form.add-card').on('submit', function(event) {
            event.preventDefault();
            var $form = $(this);
            var extraDetails = {
                name: $form.find('input[name="name"]').val(),
                address_city: $form.find('input[name="address_city"]').val(),
                address_country: $form.find('input[name="address_country"]').val(),
                address_line1 : $form.find('input[name="address_line1"]').val(),
                address_line2: $form.find('input[name="address_line2"]').val(),
                address_state: $form.find('input[name="address_state"]').val()
            };
            _stripe.createToken(_card, extraDetails).then(setOutcome);
        });

        function setOutcome( result ){
            if (result.token) {

                var post_data = {
                    client_ip           : result.token.client_ip,
                    token_id            : result.token.id,
                    stripe_id           : result.token.card.id,
                    created             : result.token.created,
                    name                : result.token.card.name,
                    address_city        : result.token.card.address_city,
                    address_country     : result.token.card.address_country,
                    address_line1       : result.token.card.address_line1,
                    address_line2       : result.token.card.address_line2,
                    address_state       : result.token.card.address_state,
                    address_zip         : result.token.card.address_zip,
                    brand               : result.token.card.brand,
                    country             : result.token.card.country,
                    exp_month           : result.token.card.exp_month,
                    exp_year            : result.token.card.exp_year,
                    last_four           : result.token.card.last4
                };
                $.ajax({
                    data: post_data,
                    dataType: 'json',
                    type: 'post',
                    success: function success( json_data ) {
                        document.location.reload( true )
                    },
                    error: DA.utils.generalFormError,
                    url:'/services/payment/add_method'
                });




            } else if (result.error) {
                console.info( result.message );
            }
        }

    };


    var _search_tail = [],
        _on_search = false,
        _$search_results;

    var _subscriptionStep = function _subscriptionStep( step ) {

        $('.form-steps').hide().addClass('hidden');
        $('.form-steps.step-'+step).removeClass('hidden').slideDown();

    };
    var _searchTail = function _searchTail( q ) {
        if( !_on_search ){
            _on_search = true;
            var data = {
                    q: q
                },
                html = '<div class="card">Buscando...</div>';
            _$search_results.html( html );
            $.ajax({
                data: data,
                dataType: 'json',
                success: function success( json_data ) {
                    if( json_data.length > 0 ){
                        html = Mustache.render('{{#items}}<div class="card"><div class="avatar"><img src="{{avatar}}" alt=""></div><div class="info"><a href="{{url}}" target="_blank">{{name}}</a></div><div class="actions text-right"><button data-id="{{id}}" data-type="{{type}}" class="btn btn-default select-target margin-top-10">Elegir para membresia</button></div></div>{{/items}}', {items:json_data});
                    }else{
                        html = '<div class="card">No hay resultados</div>';
                    }
                    _$search_results.html( html );
                    _search_tail.splice(0, 1);
                    _on_search = false;
                    if( _search_tail.length > 0 ){
                        _searchTail( _search_tail[ _search_tail.length - 1 ] );
                    }
                },
                url:'/services/doctors-hospitals.json'
            });
        }else{
            _search_tail.push( q );
        }
    };
    var _initSubscription = function _initSubscription() {

        _$search_results = $('#search-target-results');
        _$search_results.delegate('button.select-target', 'click', function ( event ) {
            event.preventDefault();

            var $button = $('button.select-target'),
                id = $button.data('id'),
                type = $button.data('type');

            $('[data-method-type]').hide();

            switch ( type ){
                case 'doctor':
                    $('[data-method-type="doctor"]').show();
                    break;
                case 'hospital':
                    $('[data-method-type="hospital"]').show();
                    break;
                default:
                    break;

            }
            $('#target_id').val( id );
            $('#target_type').val( type );
            _subscriptionStep( 2 );

        });

        $('#search-target-input').on('keyup',function () {
            var $input = $(this);
            _searchTail( $input.val()  );
        });
        $('button.select-payment').on('click', function (event) {
            event.preventDefault();


            /*
            *   $cu = \Stripe\Customer::retrieve($customer_id); // stored in your application
             $cu->source = $_POST['stripeToken']; // obtained with Checkout
             $cu->save();
            * */
            var $button = $(this),
                $cvv = $button.parent().find('[name="cvv"]'),
                id = $button.data('id'),
                cvv = $cvv.val(),
                post_data = {
                    token_id: id,
                    cvv: cvv
                };
            $.ajax({
                data: post_data,
                dataType: 'json',
                type: 'post',
                success: function success( json_data ) {
                    $('#payment_id').val( json_data.id );
                    _subscriptionStep( 3 );
                },
                error: DA.utils.generalFormError,
                url:'/services/payment/refresh_card_token'
            });


        });

        $('button.add-subscription').on('click', function (event) {
            event.preventDefault();
            var $btn = $(this),
                post_data = {
                    plan: $btn.data('plan'),
                    target_id: $('#target_id').val(),
                    target_type: $('#target_type').val(),
                    payment_id: $('#payment_id').val(),
                };

            $.ajax({
                data: post_data,
                dataType: 'json',
                type: 'post',
                success: function success( json_data ) {


                },
                error: DA.utils.generalFormError,
                url:'/services/payment/add_subscription'
            });
        });
        _subscriptionStep( 1 );
    };



    return {
        init: function init( stripe_key ) {
            _init(stripe_key);
        },
        initSubscription: function () {
            _initSubscription();
        }
    }
})();