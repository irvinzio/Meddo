DA.search = (function(){

    var _autocomplete,
        _$searchComponent,

        _search_location = {
            latitude: 0,
            longitude: 0,
            city: ''
        };


    function _geolocate( input ) {
        if( _search_location.latitude === 0 ){
            $( input ).addClass('loading');
            if ( navigator.geolocation ) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    $( input ).removeClass('loading');
                    if( _search_location.latitude === 0 ) {
                        _search_location.latitude = position.coords.latitude;
                        _search_location.longitude = position.coords.longitude;
                        $("#autocomplete").val('Más cercano a mi');
                    }
                });
            }
        }
    }


    function _fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = _autocomplete.getPlace(),
            i, j;

        _search_location.city = '';
        _search_location.latitude = place.geometry.location.lat();
        _search_location.longitude = place.geometry.location.lng();

        for ( i in place.address_components ) {
            for( j in place.address_components[i].types ){
                if( place.address_components[i].types[j] === 'locality' ){
                    _search_location.city = place.address_components[i].long_name;
                    break;
                }
            }
            if( _search_location.city !== ''){
                break;
            }
        }
        
    }

    function _initAutocomplete() {
        _autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('autocomplete'),
            {
                types: ['geocode'],
                componentRestrictions: {
                    country: 'mx'
                }
            }
        );
        _autocomplete.addListener('place_changed', _fillInAddress);
    }

    var _init = function _init() {
        _$searchComponent = $("form.search-component");
        _$searchComponent.on('submit', function( event ){
            event.preventDefault();
            event.stopImmediatePropagation();
            var $form = $(this);
            _validateSearch($form);
        });
    };


    var _validateSearch = function _validateSearch( $form ) {
        var query = $form.find('.searchTop').val();
        if( query != null )
        if( query.length > 0){
            _makeSearch( query, null );
        }else{
            DA.main.sideAlert('Ingresa un texto de búsqueda.');
        }
    };

    var _makeSearch = function _makeSearch( query, type ) {
        var url_params = [];
        if( query ){
            url_params.push('q='+ encodeURI( query.toLowerCase() ));
        }
        if( type ){
            url_params.push('type='+ encodeURI( type.toLowerCase() ));
        }
        if( _search_location.latitude !== 0 ){
            url_params.push( 'latitude='+ encodeURI( _search_location.latitude.toString() ));
            url_params.push( 'longitude='+ encodeURI( _search_location.longitude.toString() ));
        }
        if( _search_location.city !== '' ) {
            url_params.push( 'city='+ encodeURI( _search_location.city ));
        }
        document.location.href = '/#!/busqueda?'+(url_params.join('&'));
    };


    var _loads = 0;
    return {
        init: function init() {
            if(_loads === 0){
                _init();
            }else{
                _initAutocomplete();
            }
            _loads++;
        },
        input_focus: function input_focus( input ) {
            _geolocate( input );
        },
        makeSearch : function(query){
            _makeSearch( query, null );
        }
    }
})();