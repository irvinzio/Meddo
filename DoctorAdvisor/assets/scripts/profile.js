DA.profile = (function(){
    var _$this,
        _avatar_input,
        _$avatar_message,
        _$avatar_preview,
        _$gal_profile,
        _$gal_ul,
        _$gal_li
    ;

    var _initVars = function _initVars(){
        _$gal_profile = $('.profile-gal');
        _$gal_ul = $('.profile-gal ul');
        _$gal_li = $('.profile-gal ul li');

        _avatar_input = document.getElementById('avatar-input');
        _$avatar_message = $('#avatar-message');
        _$avatar_preview = $('#avatar-preview');

    };

    var _formScore = function _formScore( $form, id_list ){

        var data = {},
            ii, key, value;

        for( ii in id_list ){
            key = id_list[ ii ];
            value = parseInt( $form.find('[name="'+key+'"]').val(), 10);
            if( value > 0 ){
                data[ key ] = value;
            }else{
                data = {};
                break;
            }
        }
        return data;
    };
    var _initEvents = function _initEvents(){
        _$gal_li.on('click', function () {
            _$this = $(this);
            if(!_$gal_profile.hasClass('expanded')){
                _$gal_ul.animate({scrollLeft: _$this.offset().left}, 400);
                setTimeout(function(){
                    _$gal_profile.addClass('expanded');
                }, 300);
            }
            if(_$this.hasClass('video')){
                if(!_$this.hasClass('expanded')){
                    _$this.addClass('expanded');
                    // _$this.find('iframe').src += "&autoplay=1";
                    // _$this.find('iframe').attr("src", _$this.find('iframe').attr("src").replace("autoplay=0", "autoplay=1"));
                }
            }
        });

        $("#form-comment").on('submit', function( event ){
            event.preventDefault();
            var $form = $(this),
                type = $form.data('type'),
                id = $form.data('id'),
                title = $('#title').val(),
                comment = $('#comment').val(),
                recommendation = $('#recommendation').val(),

                data = {};

            if( type === 'doctor'){
                data = _formScore( $form, [
                    'attention_wait',
                    'good_treatment',
                    'sanitation',
                    //'professional',
                    'installations',
                    'medical_diagnostic',
                    'efficient_treatment'
                ]);
            }else{
                data = _formScore( $form, [
                    'service',
                    'good_treatment',
                    'sanitation',
                    'installations',
                    'personal'
                ]);
            }
            if( !$.isEmptyObject( data )){

                data.type = type;
                data.id = id;
                data.comment = comment;
                data.recommendation = recommendation;

                if( $('#accept-terms').is(':checked') ){
                    DA.utils.ajaxForm(data, '/services/rate', function (json_data) {
                        DA.main.sideAlert('Gracias por tu valoración.');
                        setTimeout(function(){
                            $form.closest('.close-form').slideUp(600);
                            document.location.reload( 1 );
                        }, 400);
                    });
                }else{
                    DA.main.sideAlert('Debes certificar tu opinión.');
                }
            }else{
                DA.main.sideAlert('Califica con al menos una estrella en todas las categorías.');
            }
        });

        if (_avatar_input && _avatar_input.addEventListener) {
            _avatar_input.addEventListener('change', function ( event ) {
                var i = 0,
                    formdata = new FormData(),
                    length = this.files.length,
                    img = false,
                    file;

                _$avatar_message.html('Subiendo <i class="fa fa-refresh fa-spin"></i>');

                for ( ; i < length; i++ ) {
                    file = this.files[i];
                    if (!!file.type.match(/image.*/)) {
                        formdata.append("image", file);
                        img = true;
                        break;
                    }
                }

                if ( img ) {
                    $.ajax({
                        url: "/settings/profile/avatar", // Url to which the request is send
                        type: "POST",
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function ( json_data ) {
                            _$avatar_preview.attr({src:json_data.src}).hide();
                            setTimeout(function () {
                                _$avatar_preview.fadeIn();
                            }, 500);
                            _$avatar_message.html('');

                        }
                    });
                }else{
                    _$avatar_message.html('Elije una image JPG o PNG');
                }
            }, false);
        }
    };



    var _search_tail = [],
        _on_search = false,
        _$search_results;

    var _searchTail = function _searchTail( q, type ) {
        if( !_on_search ){
            _on_search = true;
            var data = {
                    q: q
                },
                html = '<div class="card">Buscando...</div>';
            if( type ){
                data.type = type;
            }
            _$search_results.html( html );
            $.ajax({
                data: data,
                dataType: 'json',
                success: function success( json_data ) {
                    if( json_data.length > 0 ){
                        html = Mustache.render('{{#items}}<div class="card"><div class="avatar"><img src="{{avatar}}" alt=""></div><div class="info"><a href="{{url}}" target="_blank">{{name}}</a></div><div class="actions text-right"><button data-id="{{id}}" data-type="{{type}}" class="btn btn-default select-target margin-top-10">Elegir</button></div></div>{{/items}}', {items:json_data});
                    }else{
                        html = '<div class="card">No hay resultados</div>';
                    }
                    _$search_results.html( html );
                    _search_tail.splice(0, 1);
                    _on_search = false;
                    if( _search_tail.length > 0 ){
                        _searchTail( _search_tail[ _search_tail.length - 1 ] );
                    }
                },
                url:'/services/doctors-hospitals.json'
            });
        }else{
            _search_tail.push( q );
        }
    };


    var _initAggregation  = function _initAggregation() {

        _$search_results = $('#search-target-results');
        _$search_results.delegate('button.select-target', 'click', function ( event ) {
            event.preventDefault();
            var $button = $('button.select-target'),
                id = $button.data('id'),
                type = $button.data('type');
            var confirmation = prompt('¿Confirmas que eres este '+type+' o su representante?\nEscribe "Sí" para contunuar.');
            if( DA.utils.slug( confirmation ) === 'si' ){
                var post_data = {
                    id: id,
                    type: type
                };
                $.ajax({
                    data: post_data,
                    dataType: 'json',
                    type: 'post',
                    success: function success( json_data ) {
                        document.location.href = json_data.src;
                    },
                    url:'/services/add_instance'
                });
            }

        });

        $('#search-target-input').on('keyup',function () {
            var $input = $(this),
                type = $input.data('type');
            _searchTail( $input.val(), type );
        });
    };

    var _doctor_id = null;
    var _initDoctor = function _initDoctor( did ){
        _doctor_id = did;
        if( _doctor_id ){
            DA.utils.avatar_listener(
                document.getElementById('avatar-image'),
                '/settings/doctors/'+_doctor_id,
                document.getElementById('avatar-preview'),
                document.getElementById('avatar-messages')
            );
            _initGalleryAdmin();
        }

        if( window.related_options ){
            var ii;
            for( ii in related_options ){
                DA.utils.related_options(related_options[ii].base_id,related_options[ii].related_id);
            }
        }
    };


    var _hospital_id = null;
    var _initHospital = function _initHospital( hid ){
        _hospital_id = hid;
        if( _hospital_id ){
            DA.utils.avatar_listener(
                document.getElementById('avatar-image'),
                '/settings/hospitals/'+_hospital_id,
                document.getElementById('avatar-preview'),
                document.getElementById('avatar-messages')
            );
            _initGalleryAdmin();
        }
        if( window.related_options ){
            var ii;
            for( ii in related_options ){
                DA.utils.related_options(related_options[ii].base_id,related_options[ii].related_id);
            }
        }
    };
    function _initGalleryAdmin() {

        $('.item-gallery').each(function () {
            var $gallery = $(this),
                $gallery_wrapper = $gallery.children('.gallery-wrapper'),
                endpoint = $gallery.data('endpoint'),
                endpoint_delete = $gallery.data('delete'),

                add_image_input = document.getElementById('gallery-image')
            ;


            $gallery.delegate('button.add-element-video', 'click', function ( event ) {
                event.preventDefault();
                var yt_id = prompt('Ingresa el ID de youtube');
                if( yt_id ){
                    var post_data = {
                        'action' : 'upload_video',
                        'id': yt_id
                    };
                    $.ajax({
                        data: post_data,
                        url: endpoint,
                        type: 'POST',
                        success: function ( json_data ) {
                            refresh_gallery();
                        }
                    });
                }
            });
            function refresh_gallery() {
                $gallery.css({
                    'pointer-events':'none',
                    'opacity': 0.3
                });
                $.ajax({
                    success: function (json_data) {
                        var html = '',
                            id = 'gallery-' + parseInt( (new Date)*1, 10),
                            render_data,
                            ii, item;

                        for( ii in json_data.data ){
                            item = json_data.data[ii];
                            if( item.type == 'video' ){
                                html += Mustache.render( DA.templates.item_gallery_video, item);
                            }else{
                                html += Mustache.render( DA.templates.item_gallery_image, item);
                            }
                        }

                        render_data = {
                            id: id,
                            html: html
                        };
                        html = Mustache.render( DA.templates.item_gallery, render_data);
                        $gallery.css({ 'pointer-events' : 'all' }).stop().animate({ 'opacity' : 1}, 300);
                        $gallery_wrapper.html( html );


                        $('#'+id)
                            .delegate('button.delete-element', 'click', function ( event ) {
                                event.preventDefault();
                                if( confirm('¿Estás seguro?, esta acción no se puede deshacer.') ){

                                    console.info( '-------' );

                                    var $this = $(this),
                                        nid = $this.data('id'),
                                        post_data = {
                                            item_id: nid
                                        };
                                    console.info( post_data );
                                    $.ajax({
                                        data: post_data,
                                        url: endpoint_delete,
                                        type: 'POST',
                                        success: function ( json_data ) {
                                            refresh_gallery();
                                        }
                                    });
                                }
                            })
                            .owlCarousel({
                                items:1,
                                loop:true,
                                margin:5,
                                height: 250,
                                nav:true,
                                video:true,
                                lazyLoad:true,
                                center:true
                            });

                    },
                    url: endpoint
                });
            }
            DA.utils.image_gallery_listener( add_image_input, endpoint, refresh_gallery);
            refresh_gallery();
        });
    }


    return {
        init: function init() {
            _initVars();
            _initEvents();
        },
        initDoctor: function initDoctor( did ) {
            _initDoctor( did );
        },
        initHospital: function initHospital( hid ) {
            _initHospital( hid );
        },
        initAggregation: function initAggregation() {
            _initAggregation();
        }
    }
})();