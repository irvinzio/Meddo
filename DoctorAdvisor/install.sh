#!/bin/bash
clear
echo "Installing curl"
sudo apt-get install curl -y
echo "Installing nginx"
sudo apt-get -y install nginx
sudo service nginx start

#optional
# set up nginx server
sudo cp /vagrant/.provision/nginx/nginx.conf /etc/nginx/sites-available/site.conf
sudo chmod 644 /etc/nginx/sites-available/site.conf
sudo ln -s /etc/nginx/sites-available/site.conf /etc/nginx/sites-enabled/site.conf
sudo service nginx restart
