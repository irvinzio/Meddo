app.service('homeService',function($http, $q,Constants) {
    var baseurl = Constants.BaseUrl;
    var baseurl2 = Constants.BaseUrlNoVersion;
    return {
      'getCarrouselItems': function(limit) {
        var defer = $q.defer();
        $http.get(baseurl+'/search/carousel?number_records='+limit).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'registerDoctor': function(data) {
        var defer = $q.defer();
        $http.post(baseurl+'/doctor/registration', data).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'registerPacient': function(data) {
        var defer = $q.defer();
        defer.resolve(GeFakeRecord(data));
        return defer.promise;
      },
      'login': function(data) {
          var defer = $q.defer();
          $http.post(baseurl+'/user/login/', data).then(function(resp){
            defer.resolve(resp);
          },function(err) {
            console.log(err);
            defer.reject(err);
          });
          return defer.promise;
      },
      'logout': function(data) {
        var defer = $q.defer();
        $http.post(baseurl+'/user/logout/', data).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
    },
      'getAutocompleteInfo': function(pattern) {
        var defer = $q.defer(); 
        $http.get(baseurl+'/search/auto/'+pattern).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'activateAccount': function(id) {
        var defer = $q.defer(); 
        $http.get(baseurl+'/user/registration/account-confirm-email/'+id).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'sendMail': function(data) {
        var defer = $q.defer(); 
        $http.post(baseurl+'/contact',data).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'getDoctorProfile': function(id) {
        var defer = $q.defer(); 
        console.log(baseurl+'/doctor/profile?doctor_id='+id);
        $http.get(baseurl+'/doctor/profile?doctor_id='+id).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      }
  };
});


// data: {…}, status: 400, headers: ƒ, config: {…}, statusText: "Bad Request", …}
// config
// :
// {method: "POST", transformRequest: Array(1), transformResponse: Array(1), paramSerializer: ƒ, jsonpCallbackParam: "callback", …}
// data
// :
// email
// :
// Array(1)
// 0
// :
// "A user is already registered with this e-mail address."
// length
// :
// 1
// __proto__
// :
// Array(0)
// __proto__
// :
// Object
// headers
// :
// ƒ (d)
// status
// :
// 400
// statusText
// :
// "Bad Request"
// xhrStatus
// :
// "complete"
// __proto__
// :
// Object