(function(){
    'use strict';
    app.controller('homeCtrl',home);

    home.$inject=['$scope','Constants','homeService'];

    function home($scope,Constants,homeService){
        var vm = this;
        $scope.user = {isLogged : (localStorage.isLogged)?true:false,id:localStorage.userId};
        console.log($scope.user);
        vm.baseUrl = Constants.BaseUrl;
        vm.query = { pattern : null,longitude : 0,latitude:0,number_records:5,page : 1};
        vm.carrouselItemsLimit = 10;
        vm.carrouselItems = [];
        
        vm.init = function(){
            vm.getCarrouselItems(vm.carrouselItemsLimit);
            $(".shot-fancy").click( function (event) {
                event.preventDefault();
                $("#want-info").addClass('active');
                $("#topic").val($(this).data('id'));
            }); 
        };

        vm.getCarrouselItems = function(){
            homeService.getCarrouselItems(vm.carrouselItemsLimit).then(function(response){ 
                vm.carrouselItems =  response.data;
                console.log(vm.carrouselItems);
            },function(error){
                console.log(error);
            });
        };
        vm.init();

        vm.submitSearch = function(){
            console.log(vm.query.pattern.name);
            if( vm.query.pattern.name != null){
                DA.search.makeSearch( vm.query.pattern.name );
            }else{
                DA.search.makeSearch( vm.query.pattern );
            }
        };
        
        vm.getAutocompleteInfo = function(){
            homeService.getAutocompleteInfo(vm.query.pattern).then(function(response){ 
                vm.autcomplete = response.data;
            },function(error){
                console.log(error);
            });
        };
        vm.logout = function(){
            homeService.logout().then(function(response){ 
                $scope.clearLocalStorage();
            },function(error){
                console.log(error);
                DA.main.sideAlert('Hubo un error al terminar sesion intenta de nuevo');
            });
        }
        vm.goToProfile = function(){
            verifyUserLogged();
            if(localStorage.role == 'patient')
                window.location.href = '/!#/paciente/cuenta' 
            else if(localStorage.role == 'doctor');
                window.location.href = "/doctor/registro/#/"+localStorage.userId+"/step/1" 
        }
        $scope.clearLocalStorage = function(){
            localStorage.clear();
            $scope.user = {isLogged : false};
            window.location.href = "/";
        }
        
        $scope.sendMail = function(){
            homeService.sendMail(vm.mail).then(function(response){ 
                vm.autcomplete = response.data;
                DA.main.sideAlert('Se ha enviado la información correctamente');
            },function(error){
                console.log(error);
                DA.main.sideAlert('Hubo un error al enviar la informacion, intenta nuevamente');
            });
        }       
    }
})();