(function(){
    'use strict';
    app.controller('accountCtrl',account);

    account.$inject=['$scope','$routeParams','$location','homeService','Constants'];

    function account($scope,$routeParams,$location,homeService,Constants){
        var vm = this;
        var urlParams = $location.url().split('/');
        vm.activation = {success:true}
        vm.registerUser = function(){
            if(vm.registro.password1 != vm.registro.password2){
                DA.main.sideAlert("las contraseñas no coinciden");
                return;
            }
            homeService.registerDoctor(vm.registro).then(function(response){ 
                alert("revisa tu correo para activar tu cuenta");
                DA.main.sideAlert("revisa tu correo para activar tu cuenta");
                console.log(response);
            },function(error){
                console.log(error);
                if(error.data.error == null)
                    for (var property in error.data) {
                        if(error.data[property][0] != null)
                            DA.main.sideAlert(error.data[property][0]);
                    }
                if(error.data.error == "A user is already registered with this e-mail address")
                    DA.main.sideAlert('El email ya esta registrado');
                else
                    DA.main.sideAlert(error.data.error);
            });
        };

        vm.verifyLogin = function(){
            homeService.login(vm.login).then(function(response){ 
                console.log(response.data);
                localStorage.setItem("token", response.data.token);
                localStorage.setItem("isLogged", "true");
                localStorage.setItem("role", response.data.role);
                $scope.$parent.user.isLogged=true;
                $scope.$parent.user.role= response.data.role;
                if(response.data.role == "patient"){
                    localStorage.setItem("userId", response.data.patient_user.patient_id);
                    $scope.$parent.user.id = response.data.patient_user.patient_id;
                    window.location.href = Constants.frontBaseUrl+"/#!/paciente/cuenta/";
                        return;
                }else if(response.data.role == "doctor"){
                    localStorage.setItem("userId", response.data.doctor_user.doctor_id);
                    $scope.$parent.user.id = response.data.doctor_user.doctor_id;
                    if(!response.data.registration_completed){
                        window.location.href = Constants.frontBaseUrl+"/doctor/registro/#/"+response.data.doctor_user.doctor_id+"/step/1";
                        return;
                    }
                    else{
                         window.location.href = Constants.frontBaseUrl+"/doctor/dashboard/#/miagenda";
                         return;
                     }
                }else{
                    DA.main.sideAlert('usuario no reconocido');
                }
                
                                   
            },function(error){
                console.log(error);
                if(error.data.error == "E-mail is not verified.")
                        DA.main.sideAlert('Verifica tu correo para poder ingresar');
                else if( error.data.error == "Unable to log in with provided credentials.")
                    DA.main.sideAlert('Contraseña y/o correo incorrecto');
                else
                 DA.main.sideAlert(error.data.error);
            });
            
        }
        
        vm.activateAccount = function(activationId){
            homeService.activateAccount(activationId).then(function(response){ 
                vm.activation.success=true;
            },function(error){
                console.log(error);
                vm.activation.success=false;
            });
        };
        vm.init = function(){
            if($routeParams.activationId != null)
                vm.activateAccount($routeParams.activationId);
            else if(urlParams == "/registro/cuenta/activar")
                vm.activation.success=false;
        };
        vm.init();
    }
})();