(function(){
    'use strict';
    app.controller('doctorProfileCtrl',doctorProfile);

    doctorProfile.$inject=['$scope','$routeParams','Constants','homeService','locationService','CatalogService'];

    function doctorProfile($scope,$routeParams,Constants,homeService,locationService,CatalogService){
        var vm = this;
        var notfound =  "not found";
        vm.doctorId = $routeParams.doctorId;
        console.log("dcctorid");
        console.log(vm.doctorId);
        
        homeService.getDoctorProfile(vm.doctorId).then(function(response){ 
            vm.doctorProfile =  response.data;
            if(vm.doctorProfile.offices != null)
            vm.doctorProfile.selectedOffice = (vm.doctorProfile.offices.length>0)?vm.doctorProfile.offices[0]:{};
            console.log(vm.doctorProfile);
        },function(error){
            DA.main.sideAlert('Hubo un error obteniendo el perfil del doctor');
            console.log(error);
        });
        vm.updateOfficeInfo = function(office){
            vm.doctorProfile.selectedOffice = office;
        };
        vm.BuildOfficeAddress = function(office){
            var notFound = "Dirección no registrada";
            if(!vm.isOfficeRegistered(office)) return  notFound;
            var adrress = (office.address != null)?office.address:"";
            adrress += (office.colon != null)?", Col. "+office.colon:"";
            adrress += (office.exterior_number != null)?", "+office.exterior_number:"";
            adrress += (office.colony != null)?", "+office.colony:"";
            adrress += (office.interior_number != null)?", "+office.interior_number:"";
            
            return (adrress == "")?notFound:adrress;
        };
        vm.isOfficeRegistered = function(office){
            return (office == null)?false:true;
        }
        vm.getSpecialtyNameById = function(id){
            if(id ==  null || vm.specialtiesList == null)return notfound;
            var response = vm.specialtiesList.find(item => {
                return item.id_type == id;
             });
             return (response == null)?notfound: response.name;
        }
        locationService.getStates().then(function(response) {
            vm.estados = response.data;
        },function (error){
          console.log(error);
        });
        locationService.getCities().then(function(response) {
            vm.ciudades = response.data;
        },function (error){
          console.log(error);
        });
        CatalogService.getSpecialties().then(function(response) {
            vm.specialtiesList = response.data;
            console.log(vm.specialtiesList);
        },function (error){
          console.log(error);
        });
        vm.getStateNameById = function(id){
            if(id ==  null || vm.estados == null)return notfound;
            var response = vm.estados.find(item => {
                return item.id_type == id;
             });
             return (response == null)?notfound: response.name;
        }
        vm.getCityNameById = function(id){
            if(id ==  null || vm.ciudades == null)return notfound;
            var response =  vm.ciudades.find(item => {
                return item.id_type == id;
             });
             return (response == null)?notfound: response.name;
        }
        vm.getScoreArray = function(score){
            var auxscore =  new Array();
            for(var x=0;x<score;x++){
                auxscore.push(x);
            }
            return auxscore;
        }
    }
})();