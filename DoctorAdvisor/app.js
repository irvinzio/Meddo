(function () {
    app = angular.module('app', ['ngRoute','commonApp','ui.bootstrap']);

    app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider) {  
        $routeProvider
        .when('/', {
            templateUrl: 'home/home.html',
            controller: 'homeCtrl as vm'
        })
        .when('/home', {
            templateUrl: 'home/home.html',
            controller: 'homeCtrl as vm'
        })
        .when('/como-funciona', {
            templateUrl: 'home/howItWorks.html',
            controller: 'homeCtrl as vm'
        })
        .when('/servicios', {
            templateUrl: 'home/services.html',
            controller: 'homeCtrl as vm'
        })
        .when('/contacto', {
            templateUrl: 'home/contact.html',
            controller: 'homeCtrl as vm'
        })
        .when('/faqs', {
            templateUrl: 'home/faqs.html',
            controller: 'homeCtrl as vm'
        })
        .when('/anunciate', {
            templateUrl: 'home/announce.html',
            controller: 'homeCtrl as vm'
        })
        .when('/legales/aviso', {
            templateUrl: 'home/legales/privacy.html',
            controller: 'homeCtrl as vm'
        })
        .when('/legales/terminos', {
            templateUrl: 'home/legales/terms.html',
            controller: 'homeCtrl as vm'
        })
        .when('/publicidad', {
            templateUrl: 'home/adds.html',
            controller: 'homeCtrl as vm'
        })
        .when('/login', {
            templateUrl: 'home/account/login.html',
            controller: 'accountCtrl as vm'
        })
        .when('/registro/paciente', {
            templateUrl: 'home/account/registro.html',
            controller: 'accountCtrl as vm'
        })
        .when('/registro/doctor', {
            templateUrl: 'home/account/registro.html',
            controller: 'accountCtrl as vm'
        })
        .when('/registro/cuenta/activar/:activationId', {
            templateUrl: 'home/account/activationResponse.html',
            controller: 'accountCtrl as vm'
        })
        //booking
        .when('/busqueda', {
            templateUrl: 'booking/views/search.html',
            controller: 'searchCtrl as vm'
        })
        .when('/busqueda/:q', {
            templateUrl: 'booking/views/search.html',
            controller: 'searchCtrl as vm'
        })
        .when('/reservacion/horario/doctor/:doctorId/consultorio/:officeId', {
            templateUrl: 'booking/views/booking.html',
            controller: 'scheduleCtrl as vm'
        })
        .when('/reservacion/horario/doctor/:doctorId', {
            templateUrl: 'booking/views/booking.html',
            controller: 'scheduleCtrl as vm'
        })
        .when('/reservacion/pago/:idCita', {
            templateUrl: 'booking/views/PaymentMethod.html',
            controller: 'scheduleCtrl as vm'
        })
        .when('/reservacion/pago/:idCita/metodo', {
            templateUrl: 'booking/views/creditCarfInfo.html',
            controller: 'scheduleCtrl as vm'
        })
        .when('/reservacion/pago/ok', {
            templateUrl: 'booking/views/appointmentSuccess.html',
            controller: 'scheduleCtrl as vm'
        })
        //doctor profile
        .when('/perfil/doctor/:doctorId', {
            templateUrl: 'home/doctor/profile.html',
            controller: 'doctorProfileCtrl as vm'
        })
        // pacientes
        .when('/paciente/cuenta', {
            templateUrl: 'pacientes/views/profile.html',
            controller: 'settingsCtrl as vm'
        });
        
        // $locationProvider.html5Mode({
        //         enabled: true,
        //         requireBase: false
        // });
        // if(window.history && window.history.pushState){
        //     $locationProvider.html5Mode({
        //             enabled: true,
        //             requireBase: false
        //     });
        // }
    }]);    
})();