doctorApp.service('doctorService',function($http, $q,Constants) {
    var baseurl = Constants.BaseUrl;
    return {
      'editDoctor': function(data) {
        console.log(data);
        var defer = $q.defer();
        $http.put(baseurl+'/doctor/'+data.id, data,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'getDoctor': function(id) {
        var defer = $q.defer();
        $http.get(baseurl+'/doctor/'+id,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'logout': function(data) {
        var defer = $q.defer();
        $http.post(baseurl+'/user/logout/', data).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
    },
    'getDoctorProfile': function(id) {
      var defer = $q.defer(); 
      console.log(baseurl+'/doctor/profile?doctor_id='+id);
      $http.get(baseurl+'/doctor/profile?doctor_id='+id).then(function(resp){
        defer.resolve(resp);
      },function(err) {
        console.log(err);
        defer.reject(err);
      });
      return defer.promise;
    }
  };
});