(function(){
    'use strict';
    doctorApp.controller('RegisterDoctorCtrl', Register);

    Register.$inject=['$scope', '$rootScope','$location','$routeParams','DateService','locationService','CatalogService','doctorService','Constants'];

    function Register($scope, $rootScope, $location,$routeParams,DateService,locationService,CatalogService,doctorService,Constants){
        var vm = this;
        verifyUserLogged();
        vm.doctorId = $routeParams.id;
        vm.stepLocation = [vm.doctorId+"/step/1",vm.doctorId+"/step/2",vm.doctorId+"/step/3",vm.doctorId+"/step/4"];
        var urlParams = $location.url().split('/');
        vm.StepNumber = urlParams[3];
        vm.myDefaultImage = Constants.frontBaseUrl+"/assets/images/photoUploadDefault.png";
        vm.consent_agreement = false;
        vm.isRequired = true;
        vm.doctorSubmit = {};
        vm.doctor = {step1:{},step2:{},step3:{},step4:{}};
        vm.doctor.step1.profile_picture="fakeid";
        vm.doctor.step3.offices = [];
        vm.cities = [];
        vm.states = [];
        vm.tipoConsultorio = [];

        vm.dateFormats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        vm.dateFormat = vm.dateFormats[0];
        vm.popup1 = {opened: false};
        vm.bdPopup = function() {
            vm.popup1.opened = true;
        };
        vm.birthdayDate = {
            days: DateService.getDays(),
            months: DateService.getMonths(),
            years: DateService.getYears(),
        };
        vm.error = function(message){
            vm.alert.error.message = message;
            vm.alert.error.show = true;
        };
        vm.buildPhoneNumber = function(phone){
            if(phone)
            if(phone.code)
                return locationService.buildPhoneNumber(phone);
            return "Ingresa un numero";
        };
        vm.FormatDate = function(date){
            if(date)
                return DateService.getReadableDate(date);
            return "Selecciona una fecha";
        };
        vm.BoolToString = function(value){
            return (value)?"Si":"No";
        };
        CatalogService.getSpecialties().then(function(response) {
            vm.specialtiesList = response.data;
        },function (error){
          console.log(error);
        });
        CatalogService.getUniversities().then(function(response) {
            vm.universitiesList = response.data; 
        },function (error){
          console.log(error);
        });
        CatalogService.getIdTypes().then(function(response) {
            vm.tipoIdentificacion = response.data; 
        },function (error){
          console.log(error);
        });
        CatalogService.getOfficeTypes().then(function(response) {
            vm.OfficeType = response.data;
        },function (error){
          console.log(error);
        });
        locationService.getStates().then(function(response) {
            vm.estados = response.data;
        },function (error){
          console.log(error);
        });
        locationService.getCities().then(function(response) {
            vm.ciudades = response.data;
        },function (error){
          console.log(error);
        });
        vm.getUniversityNameById = function(id){
            return  vm.universitiesList.find(item => {
                return item.id_type == id;
             }).name;
        }
        vm.getSpecialtyNameById = function(id){
            return  vm.specialtiesList.find(item => {
                return item.id_type == id;
             }).name;
        }
        vm.getIdTypeNameById = function(id){
            return  vm.tipoIdentificacion.find(item => {
                return item.id_type == id;
             }).name;
        }

        vm.getIdTypeNameById = function(id){
            return  vm.tipoIdentificacion.find(item => {
                return item.id_type == id;
             }).name;
        }
        vm.getOfficeTypesNameById = function(id){
            return  vm.OfficeType.find(item => {
                return item.id_type == id;
             }).name;
        }

        vm.phoneCodes = locationService.getPhoneCodes();

        vm.loadOfficeType = function(officeIndex){
            vm.tipoConsultorio[officeIndex] =angular.copy(vm.OfficeType);
            if(vm.doctor.step3.offices,length>0){
                vm.doctor.step3.offices[officeIndex].office_type = response.data[0]; 
                vm.office.office_type = vm.tipoConsultorio[$index][0];
            } 
        }

        vm.loadStates = function(officeIndex){
            vm.states[officeIndex] = angular.copy(vm.estados);
            if(vm.doctor.step3.offices,length>0)
                vm.doctor.step3.offices[officeIndex].state = response.data[0];
        }
        
        vm.loadCities = function(stateId,officeIndex){
            locationService.getCitiestByStateId(stateId).then(function(response) {
                //the fucking service save is as string needs to map to make select value work 
                console.log(response.data);
                vm.cities[officeIndex] = response.data.map(function(x) {
                                                x.id = x.id.toString();
                                                return x;
                                            });
                // if(vm.doctor.step3.offices[officeIndex].city == 0)
                // vm.doctor.step3.offices[officeIndex].city = 0;
            },function (error){
              console.log(error);
            });
        };
        vm.getGenderString = function(gender){
            return (gender == "1")?"Femenino":"Masculino";
        }
        vm.editDoctor = function(data){
            console.log("edit doctor");
            console.log(data);
            doctorService.editDoctor(data).then(function(response) {
                console.log("doctor edited" );
            },function (error){
                console.log(error);
                DA.main.sideAlert(error.error);
                DA.main.sideAlert('Error guardando los datos intenta de nuevo');
            });
        };
        vm.sendUniversityInfo = function(value){
            vm.doctor.step4.university_info_consent = value;
            vm.doctor.step4.registration_complete = 1;
            vm.saveAndContinue(vm.StepNumber);
            $('#promotion').modal('hide');
            
        }
        vm.formatDoctorDisplay = function(doctor){
            //for step 1
            vm.doctor.step1.birthday = new Date(doctor.birthday);
            vm.doctor.step1.profile_picture = doctor.profile_picture;
            vm.doctor.step1.name = doctor.name;
            vm.doctor.step1.last_name = doctor.last_name;
            vm.doctor.step1.last_name_2 = doctor.last_name_2;
            vm.doctor.step1.curp = doctor.curp;
            vm.doctor.step1.gender = doctor.gender;
            vm.doctor.step1.email = doctor.email;
            var codeString = vm.phoneCodes[0].name;
            vm.doctor.step1.cellphone ={code:codeString,number:''};
            if(doctor.cellphone != null){
                var phone = doctor.cellphone.split(' ');
                vm.doctor.step1.cellphone = (phone.length>1)?{code:phone[0],number:phone[1]}:{code:codeString,number:phone[0]};
            }

            //for step 2
            vm.doctor.step2.university = doctor.university;
            vm.doctor.step2.speciality_id = doctor.speciality_id;
            vm.doctor.step2.cedula = doctor.cedula;
            vm.doctor.step2.document_type = doctor.document_type;
            vm.doctor.step2.document_image = doctor.document_image;

            //for step3
            vm.doctor.step3.online_consultancy = doctor.online_consultancy;
            vm.doctor.step3.gender = doctor.gender;
            vm.doctor.step3.consent_agreement =  (doctor.consent_agreement)?true:false;
            doctor.offices.forEach(function(part, index, theArray) {
                var phone = theArray[index].phone_number.split(' ');
                theArray[index].phone_number ={};
                theArray[index].phone_number = (phone.length>1)?{code:phone[0],number:phone[1]}:{code:'',number:phone[0]};
            });
            vm.doctor.step3.offices = doctor.offices;
            vm.doctor.step3.account_number = doctor.account_number;
            vm.doctor.step3.destination_bank = doctor.destination_bank;
            vm.doctor.step3.email = doctor.email;
            console.log(doctor);
            console.log("dotor display");
            console.log(vm.doctor.step3);

            vm.doctor.step4.digital_certification_notice = (doctor.digital_certification_notice)?true:false;
            vm.doctor.step4.internet_plans_notice = (doctor.internet_plans_notice)?true:false;
        };
        vm.saveAndContinue = function(stepNumber){
            switch(stepNumber){
                case '1':
                    console.log("entro alstep numero 1 ");
                    vm.doctorSubmit = angular.copy(vm.doctor.step1);
                    vm.doctorSubmit.birthday = new Date(vm.doctor.step1.birthday).toJSON().slice(0,10);
                    vm.doctorSubmit.cellphone = vm.doctor.step1.cellphone.code+' '+vm.doctor.step1.cellphone.number;
                    vm.doctorSubmit.id = vm.doctorId;
                    vm.editDoctor(vm.doctorSubmit);
                    break;

                case '2':
                    vm.doctorSubmit = angular.copy(vm.doctor.step2);
                    vm.doctorSubmit.university = vm.doctor.step2.university;
                    vm.doctorSubmit.speciality_id = vm.doctor.step2.speciality_id;
                    vm.doctorSubmit.document_type = vm.doctor.step2.document_type;
                    vm.doctorSubmit.id = vm.doctorId;
                    vm.editDoctor(vm.doctorSubmit);
                    break;
                case '3':
                    vm.doctorSubmit = angular.copy(vm.doctor.step3);
                    vm.doctorSubmit.offices.forEach(function(part, index, theArray) {
                        theArray[index].phone_number = part.phone_number.code+' '+part.phone_number.number;
                        theArray[index].state = part.state;
                        theArray[index].city = part.city;
                        theArray[index].office_type = part.office_type;
                    });
                    vm.doctorSubmit.consent_agreement = vm.doctor.step3.consent_agreement ? 1 : 0;
                    vm.doctorSubmit.id = vm.doctorId;
                    vm.editDoctor(vm.doctorSubmit);
                    break;
                break;
                case '4':
                    vm.doctorSubmit = angular.copy(vm.doctor.step4);
                    vm.doctorSubmit.digital_certification_notice = (vm.doctor.step4.digital_certification_notice)?1:0;
                    vm.doctorSubmit.internet_plans_notice = (vm.doctor.step4.internet_plans_notice)?1:0;
                    vm.doctorSubmit.university_info_consent = (vm.doctor.step4.university_info_consent)?1:0;
                    vm.doctorSubmit.id = vm.doctorId;
                    vm.editDoctor(vm.doctorSubmit);
                    window.location.href = "/doctor/dashboard";
                    break;
                default:
                    console.log("defaut nothing to do");
                break;
            }     
        };
        vm.BuildImageUrl =function(pictureId){
            if(pictureId == null) return;
            var image = pictureId.split('.');
            return Constants.BaseUrl+"/doctor/images/"+image[0];
        }
        vm.removeOffice = function(index){
            vm.doctor.step3.offices.splice(index, 1);
        };
        vm.GoNext = function(){ 
            vm.saveAndContinue(vm.StepNumber);
            changeStepStyle(vm.StepNumber);
            vm.StepNumber + 1;
            console.log(vm.StepNumber);
            window.location.href = "#"+vm.stepLocation[vm.StepNumber];
        };
        var changeStepStyle = function(stepNumber){
            for(var i = 1;vm.stepLocation.length>=i;i++){
                if(i < stepNumber){
                    $('#steph'+i).attr('class', 'stepHeaderComplete');
                    $('#stepn'+i).attr('class', 'stepComplete');
                }
                else if(i == stepNumber){
                    $('#steph'+i).attr('class', 'stepHeaderActive');
                    $('#stepn'+i).attr('class', 'stepActive');
                }else if(i > stepNumber){
                    $('#steph'+i).attr('class', 'stepHeaderInactive');
                    $('#stepn'+i).attr('class', 'stepInactive');
                } 
            }
        };
        vm.addOffice = function(){
            var phone = {
                code : "",
                number: "",
            };
            var office ={
                name : "",
                address: "",
                interior_number : "",
                exterior_number : "",
                colony : "",
                zip_code : "",
                city : "",
                state : "",
                office_type : "",
                price : "",
                phone_number : phone,
                sanitary_license : "",
                operation_notice : "",
            };

            vm.doctor.step3.offices.push(office); 
        };

        var getDoctor = function(id){
            doctorService.getDoctor(id).then(function(response) {
                console.log(response);
                vm.formatDoctorDisplay(response.data);
                if(response.data.offices==0)
                    vm.addOffice();
            },function (error){
                console.log(error);
                if(error.status == 401)
                    vm.clearLocalStorage();
                console.log("error getting the doctor " + error);
                DA.main.sideAlert('Error obteniendo su infomacion intente nuevamente');
            });
        };

        getDoctor(vm.doctorId);
        changeStepStyle(vm.StepNumber);   

        vm.logout = function(){
            doctorService.logout().then(function(response){ 
                vm.clearLocalStorage();
            },function(error){
                console.log(error);
                DA.main.sideAlert('Hubo un error al terminar sesion intenta de nuevo');
            });
        }
        vm.clearLocalStorage = function(){
            localStorage.clear();
            $scope.user = {isLogged : false};
            window.location.href = "/";
        }
        vm.cancelar = function(){
            window.location.href = "/"
        }
          
    }
})();
