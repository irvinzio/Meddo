(function () {
    doctorApp = angular.module('doctorApp', ['ngRoute','commonApp','ui.bootstrap']);  

    doctorApp.config(['$routeProvider','$httpProvider','$locationProvider',function($routeProvider,$httpProvider,$locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
        .when('/:id/step/1', {
            templateUrl: 'views/Step1.html',
            controller: 'RegisterDoctorCtrl as vm',
        })
        .when('/:id/step/2', {
            templateUrl: 'views/Step2.html',
            controller: 'RegisterDoctorCtrl as vm',
        })
        .when('/:id/step/3', {
            templateUrl: 'views/Step3.html',
            controller: 'RegisterDoctorCtrl as vm',
        })
        .when('/:id/step/4', {
            templateUrl: 'views/Step4.html',
            controller: 'RegisterDoctorCtrl as vm',
        });

    }]);   
})();
