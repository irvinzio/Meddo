(function(){
    'use strict';
    doctorApp.controller('MiAgendaCtrl', miAgenda);

    miAgenda.$inject=['$scope', '$rootScope','miAgendaService','Constants','doctorService'];

    function miAgenda($scope, $rootScope,miAgendaService,Constants,doctorService){
        var vm = this;
        verifyUserLogged();
        vm.myDefaultImage = Constants.frontBaseUrl+"/assets/images/photoUploadDefault.png";
        vm.doctorId = localStorage.userId;
        vm.weekDaysStatus = {"selected":0,"active":1,"inactive":2}
        vm.weekDays = [
          { id : 0 , name : "Domingo", status : vm.weekDaysStatus.inactive},
          { id : 1 , name : "Lunes", status : vm.weekDaysStatus.inactive},
          { id : 2 , name : "Martes", status : vm.weekDaysStatus.inactive},
          { id : 3 , name : "Miercoles", status : vm.weekDaysStatus.inactive},
          { id : 4 , name : "Jueves", status : vm.weekDaysStatus.inactive},
          { id : 5 , name : "Viernes", status : vm.weekDaysStatus.inactive},
          { id : 6 , name : "Sabado", status : vm.weekDaysStatus.inactive}
        ];
        vm.weekDayIsSelected =  function(status){
          return (status == vm.weekDaysStatus.selected)?true:false;
        }
        vm.weekDayIsInactive =  function(status){
          return (status == vm.weekDaysStatus.inactive)?true:false;
        }
        vm.weekDayIsActive =  function(status){
          return (status == vm.weekDaysStatus.active)?true:false;
        }

        vm.selectschedule = function(day,office){
          if(day.status == vm.weekDaysStatus.active) {
            DA.main.sideAlert('El dia seleccionado cuenta ya cuenta con horario.');
            console.log("El dia seleccionado cuenta ya cuenta con horario.");
            return;
          }
          if(containsObject(day.id, office.timeScheduleList.schedule.week_day)){
            //removeObject(day,office.timeScheduleList.schedule.week_day);
            office.timeScheduleList.schedule.week_day.splice(office.timeScheduleList.schedule.week_day.indexOf(day.id), 1);
            day.status = vm.weekDaysStatus.inactive;
          } 
          else{
            office.timeScheduleList.schedule.week_day.push(day.id);
            day.status = vm.weekDaysStatus.selected;
          } 
          
        }
        vm.isDaySelected =  function(selectedDays){
          return (selectedDays.length > 0)?true:false;
        }
        vm.buildUnavailableDays= function(offices){
          offices.forEach(function(office) {
            office.selectedSchedule = { availability:angular.copy(vm.weekDays), timeScheduleList: {schedule:{hours:[],week_day:[]}} };
            office.schedule.forEach(function(weekday) {
              weekday.week_day.forEach(function(wd) {
                office.selectedSchedule.availability[wd].status = vm.weekDaysStatus.active;
                });
            });
          });
      }
        vm.loadOfficeInformation = function(office,scheduledata){   
          office.selectedSchedule = { availability:angular.copy(vm.weekDays), timeScheduleList: {schedule:scheduledata} };         
          office.schedule.forEach(function(weekday) {
            var status = "";
            if(weekday.week_day.join(',')===scheduledata.week_day.join(','))
              status =  vm.weekDaysStatus.selected;
            else status = vm.weekDaysStatus.active;
            weekday.week_day.forEach(function(wd) {
              office.selectedSchedule.availability[wd].status = status;
              });
          });      
        }
        var getDoctorSchedule = function(id){
            miAgendaService.getAllTimechedule(35320).then(function(response) {
                vm.OfficeSchedule = response.data;
                vm.OfficeSchedule.selectedSchedule = vm.buildUnavailableDays(vm.OfficeSchedule);
                configDatePickers(vm.OfficeSchedule);
                console.log( vm.OfficeSchedule);
            },function (error){
                console.log(error);
                if(error.status == 401)
                    vm.clearLocalStorage();
                console.log("error getting the doctor " + error);
                DA.main.sideAlert('Error obteniendo su infomación intente nuevamente');
            });
        };
        var getDoctor = function(id){
          doctorService.getDoctor(id).then(function(response) {
              vm.doctor = response.data;
          },function (error){
              console.log(error);
              if(error.status == 401)
                  vm.clearLocalStorage();
              console.log("error getting the doctor " + error);
              DA.main.sideAlert('Error obteniendo su infomación intente nuevamente');
          });
      };
      getDoctor(vm.doctorId);
      getDoctorSchedule(vm.doctorId);
        vm.clearLocalStorage = function(){
            localStorage.clear();
            $scope.user = {isLogged : false};
            window.location.href = "/";
        }
        
        vm.addTimeToTable = function(selectedSchedule){
          var time = {
            start : getTime(vm.scheduleTime.start),
            end : getTime(vm.scheduleTime.end)
          }
          selectedSchedule.timeScheduleList.schedule.hours.push(angular.copy(time));
        }
        vm.removeTimeFromTable = function(timeScheduleList,index){
          timeScheduleList.splice(index, 1);
        }
        vm.isTimelistEmpty = function(timeScheduleList){
          return (timeScheduleList.length>0)?false:true;
        }
        vm.saveTimeSchedule = function(OfficeId,timeScheduleList){
          console.log(timeScheduleList);
          if(timeScheduleList.hasOwnProperty(exception))
          timeScheduleList.exception.start = formatDate(timeScheduleList.exception.start,"yyyy-mm-dd");
          timeScheduleList.exception.end = formatDate(timeScheduleList.exception.end,"yyyy-mm-dd");

          miAgendaService.saveTimeSchedule(vm.doctorId,OfficeId,timeScheduleList).then(function(response) {
            console.log(response);
            getDoctorSchedule(vm.doctorId);
          },function (error){
              console.log(error);
              DA.main.sideAlert('Error obteniendo su infomación intente nuevamente');
          });
        }
        
        vm.getWeekString = function(weekDay){
          var weekString  = "";
          weekDay.forEach(function(wd) {
            weekString += vm.weekDays[wd].name+', ';
          });
          return weekString.trim();
        }
        vm.getHourString = function(hours){
          var weekHourString  = "";
          hours.forEach(function(h) {
            weekHourString += h.start+" - "+h.end+";";
          });
          return weekHourString.trim();
        }
        vm.getExceptionsString = function(exception){
          if(exception.length==0) return "";
          var exceptionString  = "Excepciónes:<br/>";
          exception.forEach(function(e) {
            exceptionString += e.start+" a "+e.end+"<br/>";
          });
          return exceptionString.trim();
        }

        vm.deleteSchedule =  function(office,weekday){
          console.log(office);
          miAgendaService.removeSchedulebyWeekdays(vm.doctorId,office.id,[weekday]).then(function(response) {
            console.log(response);
            getDoctorSchedule(vm.doctorId);
          },function (error){
              console.log(error);
              DA.main.sideAlert('Error obteniendo su infomación intente nuevamente');
          });
        }

        // Exceptions datepicker
        var configDatePickers = function(offices){
          vm.datepicker = [];
          offices.forEach(function(office) {
            vm.datepicker.push(
              {
                start:{
                  opened:false,
                  bdPopup: function(){
                    this.opened = true;
                  }
                },
                end:{
                  opened:false,
                  bdPopup: function(){
                    this.opened = true;
                  }
                }
               
              }
            );
          });
          
        }
        vm.dateFormats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        vm.dateFormat = vm.dateFormats[0];
        
        //time picker sectiokn
        vm.scheduleTime = { start:new Date(), end:new Date() };

        vm.scheduleTime.hourStep = 1;
        vm.scheduleTime.minuteStep = 15;

        vm.scheduleTime.ismeridian = false;

        $scope.update = function() {
            var d = new Date();
            d.setHours( 14 );
            d.setMinutes( 0 );
            $scope.mytime = d;
        };

        // vm.scheduleTime.changed = function () {
        //     $log.log('Time changed to: ' + $scope.mytime);
        // };

        $scope.clear = function() {
            $scope.mytime = null;
        };
        // timepicker 
        $scope.today = function() {
            $scope.dt = new Date();
          };
          $scope.today();
        
          $scope.clear = function() {
            $scope.dt = null;
          };
        
          $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
          };
        
          $scope.dateOptions = {
            dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
          };
        
          // Disable weekend selection
          function disabled(data) {
            var date = data.date,
              mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
          }
        
          $scope.toggleMin = function() {
            $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
          };
        
          $scope.toggleMin();
        
          $scope.open1 = function() {
            $scope.popup1.opened = true;
          };
        
          $scope.open2 = function() {
            $scope.popup2.opened = true;
          };
        
          $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
          };
        
          $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $scope.format = $scope.formats[0];
          $scope.altInputFormats = ['M!/d!/yyyy'];
        
          $scope.popup1 = {
            opened: false
          };
        
          $scope.popup2 = {
            opened: false
          };
        
          var tomorrow = new Date();
          tomorrow.setDate(tomorrow.getDate() + 1);
          var afterTomorrow = new Date();
          afterTomorrow.setDate(tomorrow.getDate() + 1);
          $scope.events = [
            {
              date: tomorrow,
              status: 'full'
            },
            {
              date: afterTomorrow,
              status: 'partially'
            }
          ];

          vm.loadOfficeData = function(office){
            vm.SelectedOffice = newOffice;
            $('#editOfficeModal').modal('show');
         }

          var newOffice = function(){
            var phone = {
                code : "",
                number: "",
            };
            var office ={
                name : "",
                address: "",
                interior_number : "",
                exterior_number : "",
                colony : "",
                zip_code : "",
                city : "",
                state : "",
                office_type : "",
                price : "",
                phone_number : phone,
                sanitary_license : "",
                operation_notice : "",
            };
        };
    }
})();
function getTime(datetime){
  var d = new Date(datetime);
  return buildTimeDigits(d.getHours())+":"+ buildTimeDigits(d.getMinutes());
}
function buildTimeDigits(time){
  return (time<10?'0':'') + time;
}
function containsObject(obj, list) {
  var i;
  for (i = 0; i < list.length; i++) {
      if (list[i] === obj) {
          return true;
      }
    }
}
function removeObject(obj, list){
    var index = list.indexOf(obj);
    if (index > -1) {
      list.splice(index, 1);
    }
}

function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  }

  function formatDate(dDate,sMode){   yyyy-mm-dd
    var today = dDate;
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    } 
    if (sMode+""==""){
        sMode = "dd/mm/yyyy";
    }
    if (sMode == "yyyy-mm-dd"){
        return  yyyy + "-" + mm + "-" + dd + "";
    }
    if (sMode == "dd/mm/yyyy"){
        return  dd + "/" + mm + "/" + yyyy;
    }
}
