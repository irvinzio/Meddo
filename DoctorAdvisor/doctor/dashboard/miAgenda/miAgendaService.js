doctorApp.service('miAgendaService',function($http, $q,Constants) {
    var baseurl = Constants.BaseUrl;
    return {
      'getAllTimechedule': function(DoctorId) {
        var defer = $q.defer();

        $http.get(baseurl+'/doctor/'+DoctorId+"/office/schedule",getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'getTimeOfficeSchedule': function(DoctorId,OfficeId, data) {
        var defer = $q.defer();

        $http.get(baseurl+'/doctor/'+DoctorId+"/office/"+OfficeId+"/schedule", data,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'saveTimeSchedule': function(DoctorId,OfficeId, data) {
        var defer = $q.defer();
        $http.post(baseurl+'/doctor/'+DoctorId+"/office/"+OfficeId+"/schedule", data,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'updateTimeSchedule': function(DoctorId,OfficeId, data) {
        var defer = $q.defer();
        $http.put(baseurl+'/doctor/'+DoctorId+"/office/"+OfficeId+"/schedule", data,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'removeSchedulebyWeekdays': function(DoctorId,OfficeId, weekList) {
        var defer = $q.defer();
        $http.delete(baseurl+'/doctor/'+DoctorId+"/office/"+OfficeId+"/schedule", weekList,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      }
  };
});