doctorApp.service('miPerfilService',function($http, $q,Constants) {
    var baseurl = Constants.BaseUrl;
    return {
      'editDoctor': function(data) {
        console.log(data);
        var defer = $q.defer();
        $http.put(baseurl+'/doctor/'+data.id, data,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
  };
});