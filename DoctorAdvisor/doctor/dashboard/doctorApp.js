(function () {
    doctorApp = angular.module('doctorApp', ['ngRoute','commonApp','ui.bootstrap','ngSanitize']);  

    doctorApp.config(['$routeProvider','$httpProvider','$locationProvider',function($routeProvider,$httpProvider,$locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
        .when('/', {
            templateUrl: 'miAgenda/miAgenda.html',
            controller: 'MiAgendaCtrl as vm'
        })
        .when('/miagenda', {
            templateUrl: 'miAgenda/miAgenda.html',
            controller: 'MiAgendaCtrl as vm'
        })
        .when('/miperfil', {
            templateUrl: 'miPerfil/miPerfil.html',
            controller: 'MiPerfilCtrl as vm'
        });
        
    }]);   
})();
