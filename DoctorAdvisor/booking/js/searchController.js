(function(){
    'use strict';
    app.controller('searchCtrl',search);

    search.$inject=['$scope','$routeParams','$location','searchService','Constants','CatalogService','locationService','homeService'];

    function search($scope,$routeParams,$location,searchService,Constants,CatalogService,locationService,homeService){
        var vm = this;
        vm.myDefaultImage = Constants.frontBaseUrl+"/assets/images/RegistrateDoctor.png";
        vm.doctorsResult = [];
        vm.query = { number_records : 5,page : ($routeParams.page!=null)?$routeParams.page:1, longitude : $routeParams.longitude, latitud:$routeParams.latitud, pattern : $routeParams.q};
        vm.pagesNumber = new Array(1);
        vm.pagination = {totalItems:0,currentPage:1,itemsPerPage:5};
        vm.init = function(){
            vm.searchDoctors(vm.query);
        };
        vm.validateSearch =  function(){
            vm.query.pattern = (vm.query.pattern.name != null)?vm.query.pattern.name:vm.query.pattern;
            //$location.path('busqueda?'+vm.query.pattern, false);
                vm.searchDoctors( vm.query );
        }
        vm.searchDoctors = function(query){
            searchService.searchDoctors(query).then(function(response){ 
                vm.pagination.totalItems = response.data.total;
                console.log(vm.pagination);
                vm.doctorsResult =  response.data.data.map(doctor =>{
                    console.log(doctor);
                    if(doctor.offices != null)
                        doctor.selectedOffice = (doctor.offices.length>0)?doctor.offices[0]:{};
                    return doctor;
                });
            },function(error){
                vm.doctorsResult =[];
                vm.pagination.totalItems = 0;
                console.log(error);
            });
        };
        CatalogService.getSpecialties().then(function(response) {
            vm.specialtiesList = response.data;
        },function (error){
          console.log(error);
        });
        CatalogService.getUniversities().then(function(response) {
            vm.universitiesList = response.data; 
        },function (error){
          console.log(error);
        });
        locationService.getStates().then(function(response) {
            vm.estados = response.data;
        },function (error){
          console.log(error);
        });
        locationService.getCities().then(function(response) {
            vm.ciudades = response.data;
        },function (error){
          console.log(error);
        });
        vm.getSpecialtyNameById = function(id){
            return  vm.specialtiesList.find(item => {
                return item.id_type == id;
             }).name;
        }
        vm.pageChanged =  function(){
            vm.query.page = vm.pagination.currentPage;
             vm.searchDoctors(vm.query);
        };
        vm.BuildOfficeAddress = function(office){
            var adrress = (office.address != null)?office.address:"";
            adrress += (office.colon != null)?", Col. "+office.colon:"";
            adrress += (office.exterior_number != null)?", "+office.exterior_number:"";
            adrress += (office.colony != null)?", "+office.colony:"";
            adrress += (office.interior_number != null)?", "+office.interior_number:"";
            
            return (adrress == "")?"Dirección no registrada":adrress;
        };
        vm.updateOfficeInfo = function(office, index){
            vm.doctorsResult[index].selectedOffice = office;
        };
        vm.getAutocompleteInfo = function(){
            homeService.getAutocompleteInfo(vm.query.pattern).then(function(response){ 
                vm.autcomplete = response.data;
            },function(error){
                console.log(error);
            });
        };
        vm.BuildImageUrl =function(pictureId){
            var image = pictureId.split('.');
            return Constants.imageBaseUrl+"/"+image[0];
        }
        vm.init();
        vm.getScoreArray = function(score){
            var auxscore =  new Array();
            for(var x=0;x<score;x++){
                auxscore.push(x);
            }
            return auxscore;
        }
    };
})();