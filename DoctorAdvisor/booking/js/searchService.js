app.service('searchService',function($http, $q,Constants) {
    var baseurl = Constants.BaseUrl;
    return {
      'searchDoctors': function(params) {
        var query="";
        for (var property in params) {
          query += "&"+property+"="+params[property];
        }
        query = query.substr(1);
        var defer = $q.defer(); 
        console.log(baseurl+'/search?'+query);
        $http.get(baseurl+'/search?'+query).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'getAutocompleteInfo': function(pattern) {
        var defer = $q.defer(); 
        $http.get(baseurl+'/search/auto/'+pattern).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'getDoctorById': function(id) {
        var defer = $q.defer();
        $http.get(baseurl+'/doctor/'+id).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      }
  };
});