(function(){
    'use strict';
    app.controller('paymentCtlr',payment);

    payment.$inject=['$scope','$routeParams','$location','ScheduleService','DateService','CatalogService','locationService','Constants'];

    function payment($scope, $routeParams,$location,ScheduleService,DateService,CatalogService,locationService,Constants){
        var vm = this;
        vm.myDefaultImage = Constants.frontBaseUrl+"/assets/images/RegistrateDoctor.png";
        
        ScheduleService.getSchedule(doctorId,officeId,date).then(function(response) {
            vm.doctor = response.data;
            vm.doctor.selectedOffice =  vm.doctor.offices.find(office => {
                                            if(office.id == officeId)
                                                return office;
                                        });
            if(vm.doctor.selectedOffice != null)
                vm.doctor.selectedOffice.schedule = (vm.doctor.selectedOffice==null && vm.doctor.length>0) ? vm.buildWeekSchedule(vm.doctor.offices[0].schedule):vm.buildWeekSchedule(vm.doctor.selectedOffice.schedule);
            console.log(vm.doctor.selectedOffice); 
        },function (error){
          console.log(error);
        });
        vm.weekDays = DateService.getWeekDays();
        vm.months = DateService.getMonths();
        
        CatalogService.getSpecialties().then(function(response) {
            vm.specialtiesList = response.data;
        },function (error){
          console.log(error);
        });
        CatalogService.getUniversities().then(function(response) {
            vm.universitiesList = response.data; 
        },function (error){
          console.log(error);
        });
        locationService.getStates().then(function(response) {
            vm.estados = response.data;
        },function (error){
          console.log(error);
        });
        locationService.getCities().then(function(response) {
            vm.ciudades = response.data;
        },function (error){
          console.log(error);
        });
        vm.getSpecialtyNameById = function(id){
            if(id == null) return;
            return  vm.specialtiesList.find(item => {
                return item.id_type == id;
             }).name;
        }

        vm.BuildOfficeAddress = function(office){
            if(office==null) return;
            var adrress = (office.address != null)?office.address:"";
            adrress += (office.colon != null)?", Col. "+office.colon:"";
            adrress += (office.exterior_number != null)?", "+office.exterior_number:"";
            adrress += (office.colony != null)?", "+office.colony:"";
            adrress += (office.interior_number != null)?", "+office.interior_number:"";
            
            return (adrress == "")?"Dirección no registrada":adrress;
        };
        vm.updateOfficeInfo = function(office){
            vm.doctor.selectedOffice = office;
            officeId = office.id;
            vm.doctor.selectedOffice.schedule = vm.buildWeekSchedule(office.schedule);
            //console.log(vm.doctor.selectedOffice);
        };
        vm.buildWeekSchedule = function(schedule){
            return vm.weekDays.map(days => {
                 return schedule.find(schedule => {
                    if(schedule.week_day == days.id){
                       return schedule;
                    }  
                    else
                        return null;
                    });
            });
        };
        vm.isScheduleAvailable = function(schedule){
            if(schedule != null)
                return true;
            return false;
        };
        vm.nextWeek = function(){
            var index = vm.weekDays.length-1;
            var day = (vm.weekDays[index].day)+1;
            vm.weekDays = DateService.setWeekDays(vm.weekDays[index].year,vm.weekDays[index].month,day);
        };
        vm.previousWeek = function(){
            var day = (vm.weekDays[0].day)-7;
            
            vm.weekDays = DateService.setWeekDays(vm.weekDays[0].year,vm.weekDays[0].month,day);
        };
        vm.updateSelectedSchedule = function(element,currentSchedule,date){
            if(userInfo == null || userInfo.role != "patient" )
            {
                DA.main.sideAlert('Necesitas hacer log in como paciente para agendar una cita');
                return;
            }  
            vm.selectedDate = element.$index+'_'+element.$id;
            vm.appointmentSelection = {
                patient_id : localStorage.userId,
                doctor_id : doctorId,
                office_id : officeId,
                start_time : currentSchedule.start,
                end_time : currentSchedule.end,
                date:  date
            };
            vm.selectedDate = element.$index+'_'+element.$id;
        }
        vm.saveAppointment = function(){
            ScheduleService.saveAppointment(vm.appointmentSelection).then(function(response) {
                $location.url('/reservacion/pago/'+response.data.data.id);
            },function (error){
                console.log(error);
                $('#comfirmBooking').modal('hide'); 
                DA.main.sideAlert('Hubo un un error al guardar la cita intenta nuevamente');
            });
        }
        vm.updateAppointment = function(){
            ScheduleService.updateAppointment(vm.appointmentSelection).then(function(response) {
                DA.main.sideAlert('Se guardaron los cambios correctamente');
                $location.url('/reservacion/pago/:idCita'); 
            },function (error){
                DA.main.sideAlert('Hubo un un error al actualizar');
            });
        }
        vm.validateSchedule =  function(){
            if(!verifyPatientLogged()){
                DA.main.sideAlert("Necesitas iniciar sesión como paciente para agendar una cita");
            }
            else if(vm.selectedDate != null){
                $('#comfirmBooking  ').modal('show'); 
            }    
            else DA.main.sideAlert("selecciona un horario");
            
        };

        vm.validateConfirmation =  function(){
            if(vm.TermsCheckbox)
                vm.updateAppointment();
            else DA.main.sideAlert("Acepta los terminos y condiciones para continuar");
        };
        vm.BuildImageUrl =function(pictureId){
            if(pictureId == null) return;
            var image = pictureId.split('.');
            return Constants.imageBaseUrl+"/"+image[0];
        }
        // vm.saveCardData() = function(){

        // }  
        // vm.cancelAppointment() = function(){
        //     ScheduleService.updateAppointment(vm.appointmentSelection).then(function(response) {
        //         $location.url('/busqueda'); 
        //     },function (error){
        //         DA.main.sideAlert('Hubo un un error al actualizar ');
        //     });
        // }       
    }
})();