app.service('ScheduleService', sheduleService);

function sheduleService($http,$q,Constants){
    var baseurl = Constants.BaseUrl;
    return{
        getDoctor: function(doctorId,officeId){
            var defered = $q.defer();
            $http.get(baseurl+'/appointment/schedule?doctor='+doctorId+'&office='+officeId).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        getSchedule: function(doctorId,officeId,date){
            var defered = $q.defer();
            $http.get(baseurl+'/appointment/schedule?doctor='+doctorId+'&office='+officeId+'&date='+date).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        saveAppointment: function(appointment){
            var defered = $q.defer();
            $http.post(baseurl+'/appointment',appointment,getHeadersConfig()).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        },
        updateAppointment: function(appointment){
            var defered = $q.defer();
            $http.put(baseurl+'/appointment/'+appointment.id,appointment,getHeadersConfig()).then(function(resp){
                defered.resolve(resp);
            },function(err) {
                console.log(err);
                defered.reject(err);
            });
            return defered.promise;
        }
    }
}
function getTestDoctor()
{
    return {
        "id": 35320,
        "name": "sdas",
        "last_name": "asda",
        "last_name_2": "Montiel",
        "gender": "1",
        "profile_picture": "dasad",
        "birthday": null,
        "city": "222",
        "rfc": "dsadsa",
        "email": "asdad@asdasd.com",
        "cellphone": "21331",
        "document_image": "sadsa",
        "document_type": 1,
        "university": 2,
        "speciality_id": 40,
        "curp": "sad",
        "cedula": "sad",
        "cedula_speciality": "dsad",
        "online_consultancy": 0,
        "digital_certification_notice": 0,
        "internet_plans_notice": 0,
        "active": 0,
        "registration_complete": 1,
        "consent_agreement": 1,
        "university_info_consent": 0,
        "insurances": [],
        "offices": [
            {
                "id": 9,
                "name": "Prueba",
                "address": "saddada",
                "address2": "saddsad",
                "interior_number": 2122,
                "exterior_number": "22",
                "colony": "dsas",
                "zip_code": 221,
                "city": "bola",
                "state": 3,
                "country": "mex2",
                "office_type": 2,
                "price": "1202.98",
                "phone_number": "213132",
                "sanitary_license": "2312",
                "operation_notice": "2312",
                "procedures": [],
                "schedule": 
                [
                    {
                        "week_day": 1,
                        "date": "20180806",
                        "hours": [
                            {
                                "start": 9,
                                "end": 10,
                                "available": 0
                            },
                            {
                                "start": 10,
                                "end": 11,
                                "available": 1
                            },
                            {
                                "start": 11,
                                "end": 12,
                                "available": 1
                            },
                            {
                                "start": 12,
                                "end": 13,
                                "available": 1
                            },
                            {
                                "start": 13,
                                "end": 14,
                                "available": 1
                            },
                            {
                                "start": 14,
                                "end": 15,
                                "available": 1
                            },
                            {
                                "start": 15,
                                "end": 16,
                                "available": 1
                            },
                            {
                                "start": 16,
                                "end": 17,
                                "available": 1
                            },
                            {
                                "start": 17,
                                "end": 18,
                                "available": 1
                            },
                            {
                                "start": 18,
                                "end": 19,
                                "available": 1
                            },
                            {
                                "start": 19,
                                "end": 20,
                                "available": 1
                            },
                            {
                                "start": 20,
                                "end": 21,
                                "available": 1
                            },
                            {
                                "start": 21,
                                "end": 22,
                                "available": 1
                            }
                        ]
                    },
                    {
                        "week_day": 2,
                        "date": "20180807",
                        "hours": [
                            {
                                "start": 9,
                                "end": 10,
                                "available": 1
                            },
                            {
                                "start": 10,
                                "end": 11,
                                "available": 1
                            },
                            {
                                "start": 11,
                                "end": 12,
                                "available": 1
                            },
                            {
                                "start": 12,
                                "end": 13,
                                "available": 1
                            },
                            {
                                "start": 13,
                                "end": 14,
                                "available": 1
                            },
                            {
                                "start": 14,
                                "end": 15,
                                "available": 1
                            },
                            {
                                "start": 15,
                                "end": 16,
                                "available": 1
                            },
                            {
                                "start": 16,
                                "end": 17,
                                "available": 1
                            },
                            {
                                "start": 17,
                                "end": 18,
                                "available": 1
                            },
                            {
                                "start": 18,
                                "end": 19,
                                "available": 1
                            },
                            {
                                "start": 19,
                                "end": 20,
                                "available": 1
                            },
                            {
                                "start": 20,
                                "end": 21,
                                "available": 1
                            },
                            {
                                "start": 21,
                                "end": 22,
                                "available": 1
                            }
                        ]
                    },
                    {
                        "week_day": 3,
                        "date": "20180808",
                        "hours": [
                            {
                                "start": 9,
                                "end": 10,
                                "available": 1
                            },
                            {
                                "start": 10,
                                "end": 11,
                                "available": 1
                            },
                            {
                                "start": 11,
                                "end": 12,
                                "available": 1
                            },
                            {
                                "start": 12,
                                "end": 13,
                                "available": 1
                            },
                            {
                                "start": 13,
                                "end": 14,
                                "available": 1
                            },
                            {
                                "start": 14,
                                "end": 15,
                                "available": 0
                            },
                            {
                                "start": 15,
                                "end": 16,
                                "available": 1
                            },
                            {
                                "start": 16,
                                "end": 17,
                                "available": 1
                            },
                            {
                                "start": 17,
                                "end": 18,
                                "available": 1
                            },
                            {
                                "start": 18,
                                "end": 19,
                                "available": 1
                            },
                            {
                                "start": 19,
                                "end": 20,
                            }
                        ]
                    }
                ]
            }
        ]
    }
}