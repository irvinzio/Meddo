(function () {
    bookingApp = angular.module('bookingApp', ['ngRoute','commonApp','ui.bootstrap']);  

    bookingApp.config(['$routeProvider','$httpProvider',function($routeProvider,$httpProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'views/search.html',
            controller: 'searchCtrl as vm',
        })
        .when('/busqueda', {
            templateUrl: 'views/search.html',
            controller: 'searchCtrl as vm',
        })
        .when('/busqueda/:q', {
            templateUrl: 'views/search.html',
            controller: 'searchCtrl as vm',
        })
        .when('/schedule/:id', {
            templateUrl: 'views/booking.html',
            controller: 'scheduleCtrl as vm',
        })
        .when('/comfirmBooking/:id', {
            templateUrl: 'views/comfirmBooking.html',
            controller: 'BookingCtrl as vm',
        })
        .when('/payment', {
            templateUrl: 'views/PaymentMethod.html',
            controller: 'BookingCtrl as vm',
        })
        .when('/paymentMethod', {
            templateUrl: 'views/creditCarfInfo.html',
            controller: 'BookingCtrl as vm',
        })
        .when('/paymentSuccess', {
            templateUrl: 'views/appointmentSuccess.html',
            controller: 'BookingCtrl as vm',
        });
    }]);   
})();