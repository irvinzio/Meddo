(function(){
    'use strict';
    app.controller('settingsCtrl',settings);

    settings.$inject=['$scope','$routeParams','CatalogService','locationService','pacientService','Constants'];

    function settings($scope,$routeParams,CatalogService,locationService,pacientService,Constants){
        var vm = this;
        verifyUserLogged();
        vm.myDefaultImage = Constants.frontBaseUrl+"/assets/images/photoUploadDefault.png";
        vm.pacientId = localStorage.userId;

        vm.getPacient = function(id){
            pacientService.getPacient(id).then(function(response){ 
                vm.pacient =  response.data;
                $scope.$parent.user.profilePicture = vm.BuildImageUrl(response.data.profile_picture);
                console.log(vm.pacient)
            },function(error){
                console.log(error);
                if(error.status== 401)
                    $scope.$parent.clearLocalStorage();
            });
        };
        vm.updatePacient = function(){
            pacientService.updatePacient(vm.pacient).then(function(response){ 
                vm.pacient =  response.data;
                DA.main.sideAlert('Se actualizo la informacion correctamente.');
            },function(error){
                console.log(error);
                DA.main.sideAlert(error.error);
                DA.main.sideAlert('Hbo un error actualizando la informacion');s
            });
        };
        vm.updatePassword = function(){
            pacientService.changePassword(vm.changePassword).then(function(response){ 
                DA.main.sideAlert('Se actualizo la informacion correctamente.');
            },function(error){
                console.log(error);
                DA.main.sideAlert(error.error);
                DA.main.sideAlert('Hbo un error actualizando la informacion');
            });
        };

        
        CatalogService.getSpecialties().then(function(response) {
            vm.specialtiesList = response.data;
        },function (error){
          console.log(error);
        });
        CatalogService.getUniversities().then(function(response) {
            vm.universitiesList = response.data; 
        },function (error){
          console.log(error);
        });
        locationService.getStates().then(function(response) {
            vm.estados = response.data;
        },function (error){
          console.log(error);
        });
        locationService.getCities().then(function(response) {
            vm.ciudades = response.data;
        },function (error){
          console.log(error);
        });
        vm.getSpecialtyNameById = function(id){
            return  vm.specialtiesList.find(item => {
                return item.id_type == id;
             }).name;
        }
        vm.BuildImageUrl =function(pictureId){
            if(pictureId == null) return;
            var image = pictureId.split('.');
            return Constants.BaseUrl+"/patient/images/"+image[0];
        }
        vm.getPacient(vm.pacientId);
    }
    //DA.main.sideAlert('dahsboard de doctor no disponible');
})();