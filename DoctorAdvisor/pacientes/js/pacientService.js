app.service('pacientService',function($http, $q,Constants) {
    var baseurl = Constants.BaseUrl;
    return {
      'getPacient': function(id) {
        var defer = $q.defer();
        $http.get(baseurl+'/patient/'+id,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'updatePacient': function(pacient) {
        var defer = $q.defer();
        $http.put(baseurl+'/patient/'+pacient.id,pacient,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      },
      'changePassword': function(passwrd) {
        var defer = $q.defer();
        $http.post(baseurl+'/user/password/change',passwrd,getHeadersConfig()).then(function(resp){
          defer.resolve(resp);
        },function(err) {
          console.log(err);
          defer.reject(err);
        });
        return defer.promise;
      }          
  };
});