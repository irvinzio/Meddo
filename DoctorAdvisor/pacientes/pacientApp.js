(function () {
    pacientApp = angular.module('pacientApp', ['ngRoute','commonApp']);  

    pacientApp.config(['$routeProvider','$httpProvider',function($routeProvider,$httpProvider) {
        $routeProvider
        .when('profile/:id', {
            templateUrl: 'views/profile.html',
            controller: 'searchCtrl as vm',
        });
    }]);   
})();